# HReader

#### 介绍
“荔枝”小说阅读器是使用Jsoup解析[天籁小说网](https://www.23txt.com/)获取小说源实现追书收藏，支持四种翻页模式、夜间模式、字体大小/阅读背景设置满足基本小说阅读器需求。

#### 软件架构
项目基于RxJava+ Retrofit2+MVP模式项目所有数据来源均属于[天籁小说网](https://www.23txt.com/)，纯属学习使用不得用于商业！！！！！！！！！！！！

#### 项目资源下载
 **APK: **[荔枝小说](https://gitee.com/shuaichuxinggaodu/HReader/blob/master/app/release/app-release.apk) 
如需其他资源可以直接下载项目源码。

#### 项目截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0323/160825_599d0e76_1119215.png "捕获.PNG")

#### 其他说明

读页框架使用：https://github.com/glongdev/Reader
该项目仅供学习不适用于商业项目如需使用请结合自己的情况适当修改。
作者联系方式：1789798360@qq.com
