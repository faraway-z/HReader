package com.xiaohou.hreader;

public class Constant {
    public static final String BASE_URL = "https://www.23txt.com";
    public static final String BOOK_NAME = "book_name";
    public static final String BOOK_PATH = "book_path";
    public static final String BOOK_DATA = "book_data";
    public static final String BOOK_CHAPTER_POSITION = "book_chapter_position";
    public static final String CHARSET = "charset";
    public static final String UTF8 = "utf-8";
    public static final String GBK = "gbk";
    public static final String SEARCH_URL = "https://www.23txt.com/search.php?keyword=";
    public static final String SEARCH_TYPE = "search_type";
    public static final String NIGHT_MDE_OPEN = "night_mde_open";
    public static final String FONT_SIZE = "font_size";
    public static final String READ_BG = "read_bg";
    public static final String EFFECT_TYPE = "effect_type";
    public static final int DEFAULT_PAGE = 0;//默认翻页
    public static final int EFFECT_OF_COVER_PAGE = 1;//覆盖翻页
    public static final int NOTHING_PAGE = 2;//无翻页模式
    public static final int SLIDE_PAGE = 3;//滑动翻页
    public static final int EMULATION_PAGE = 4;//仿真翻页
    public static final String READ_BG_0 = "#f7f3ef";
    public static final String READ_BG_1 = "#d8c9aa";
    public static final String READ_BG_2 = "#cce8cf";
    public static final String READ_BG_3 = "#b69b94";
    public static final String READ_BG_4 = "#FFFFFF";
    public static final String PAGE_TYPE = "page_type";
    public static final String BUGLY_APP_ID = "796625d643";
    public static final String UM_APP_ID = "5e69fb7f895cca6e03000040";
    public static final String   APP_CHANNEL = "百度";
}
