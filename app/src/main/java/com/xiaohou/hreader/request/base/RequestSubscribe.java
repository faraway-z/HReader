package com.xiaohou.hreader.request.base;
import com.blankj.utilcode.util.LogUtils;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * 创建时间: 2018/6/20
 * 作者: xiaoHou
 * E-mail: 605322850@qq.com
 * Blog: www.xiaohoutongxue.cn
 * 描述: RequestSubscribe
 **/
public abstract class RequestSubscribe<T> implements Observer<T> {

    private static final String TAG = "RequestSubscribe：";

    @Override
    public void onSubscribe(Disposable d) {
        onNetSubscribe(d);
    }

    @Override
    public void onNext(T t) {
        onRequestSuccess(t);
    }

    @Override
    public void onError(Throwable t) {
        RetrofitException.ResponeThrowable responeThrowable = RetrofitException.retrofitException(t);
        switch (responeThrowable.code) {
            case RetrofitException.ERROR.HTTP_ERROR:
            case RetrofitException.ERROR.SSL_ERROR:
            case RetrofitException.ERROR.NETWORD_ERROR:
            case RetrofitException.ERROR.UNKNOWN:
                onNetWorkError();
                break;
            default:
                onRequestError(responeThrowable);
                break;
        }
        LogUtils.e(TAG+"请求错误___"+responeThrowable.message);
    }


    @Override
    public void onComplete() {
    }

    /**
     * 请求数据成功
     *
     * @param response
     */
    protected abstract void onRequestSuccess(T response);

    /**
     * 请求错误
     *
     * @param objectBean
     */
    protected abstract void onRequestError(Object objectBean);

    /**
     * 网络错误
     */
    protected abstract void onNetWorkError();

    /**
     * 网络请求订阅
     *
     * @param d 可以在此取消网络请求
     */
    protected void onNetSubscribe(Disposable d) {
    }

    ;

}

