package com.xiaohou.hreader.request.base;

import com.xiaohou.hreader.Constant;

import io.reactivex.Observable;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface Api {
    /**
     * 获取主页数据
     * @return
     */
    @GET(Constant.BASE_URL+"/")
    Observable<String> getIndexData();

    /**
     * 获取书籍详情
     * @param path
     * @return
     */
    @GET
    Observable<String> getBookDetailsData(@Url String path);

    /**
     * 获取书籍内容
     * @param contentUrl
     * @return
     */
    @GET
    Observable<String> getContentData(@Url String contentUrl);

    /**
     * 搜索
     * @param keyWord
     * @return
     */
    @GET
    Observable<String> onSearchData(@Url String keyWord);
}
