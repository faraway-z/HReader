package com.xiaohou.hreader.request.bean;

public class BookContentBean {
    private String content;

    public String getContent() {
        return content;
    }

    public BookContentBean(String content) {
        this.content = content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
