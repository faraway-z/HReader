package com.xiaohou.hreader.request.mvp.index;

import com.xiaohou.hreader.request.base.RequestSubscribe;
import com.xiaohou.hreader.request.base.RxLifeCycleUtils;
import com.xiaohou.hreader.request.base.RxThreadUtil;
import com.xiaohou.hreader.request.bean.IndexBean;
import com.xiaohou.hreader.ui.base.BasePresenter;

public class IndexPersenter extends BasePresenter<IndexConcrat.view> implements IndexConcrat.persenter {
    public IndexMode mode ;

    public IndexPersenter() {
        mode = new IndexMode();
    }

    @Override
    public void getIndexData() {
        if (!isViewAttached()){
            return;
        }
        mode.getIndexData().compose(RxThreadUtil.rxObservableSchedulerHelper())
                .compose(RxLifeCycleUtils.bindToLifecycle(mView))
                .subscribe(new RequestSubscribe<String>() {
                    @Override
                    protected void onRequestSuccess(String response) {
                        if (response.equals("")){
                            mView.onError("");
                            return;
                        }
                        mView.hideLoading();
                        IndexBean indexBean = ParseIndexDataUtlis.getIndexData(response);
                        mView.onSuccess(indexBean);
                    }

                    @Override
                    protected void onRequestError(Object objectBean) {
                        mView.hideLoading();
                    }

                    @Override
                    protected void onNetWorkError() {
                        mView.hideLoading();
                    }
                });
    }
}
