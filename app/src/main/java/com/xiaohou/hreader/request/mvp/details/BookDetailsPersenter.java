package com.xiaohou.hreader.request.mvp.details;

import com.xiaohou.hreader.Constant;
import com.xiaohou.hreader.request.base.RequestSubscribe;
import com.xiaohou.hreader.request.base.RxLifeCycleUtils;
import com.xiaohou.hreader.request.base.RxThreadUtil;
import com.xiaohou.hreader.ui.base.BasePresenter;

public class BookDetailsPersenter extends BasePresenter<BookDetailsConcrat.view> implements BookDetailsConcrat.persenter {
   public BookDetailsMode mode ;

    public BookDetailsPersenter() {
        mode = new BookDetailsMode();
    }

    @Override
    public void getBookDetailsData(boolean isSearch,String path) {
        if (!isViewAttached()){
            return;
        }

        mode.getBookDetailsData(isSearch == true ?path:Constant.BASE_URL+path)
                .compose(RxLifeCycleUtils.bindToLifecycle(mView))
                .compose(RxThreadUtil.rxObservableSchedulerHelper())
                .subscribe(new RequestSubscribe<String>() {
                    @Override
                    protected void onRequestSuccess(String response) {
                        mView.onSuccess(ParseDetailsDataUtils.getBookDetailsData(response));
                    }

                    @Override
                    protected void onRequestError(Object objectBean) {

                    }

                    @Override
                    protected void onNetWorkError() {

                    }
                });

    }
}
