package com.xiaohou.hreader.request.mvp.search;

import com.blankj.utilcode.util.LogUtils;
import com.xiaohou.hreader.request.bean.BookInfoBean;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class ParseSearchData {
    private static final String TAG = "ParseSearchData：";

    public static List<BookInfoBean> onSearchData(String response) {
        List<BookInfoBean> mSearchList = new ArrayList<>();
        Document mSearchData = Jsoup.parse(response);
        Elements mResultElement = mSearchData.getElementsByClass("result-item");
        for (Element search : mResultElement) {
            BookInfoBean bookInfoBean = new BookInfoBean();
            String href = search.getElementsByClass("result-game-item-pic").select("a").attr("href");
            bookInfoBean.setRoute(href);
            String src = search.getElementsByClass("result-game-item-pic").select("img").attr("src");
            bookInfoBean.setImgUrl(src);
            Elements mInfoElements = search.getElementsByClass("result-game-item-detail");
            for (Element mInfo : mInfoElements) {
                String nameElement = mInfo.getElementsByClass("result-item-title").select("a").get(0).getElementsByTag("span").text();
                bookInfoBean.setName(nameElement);
                String info = mInfo.getElementsByTag("p").text();
                bookInfoBean.setInfo(info);
            }
            String author = search.getElementsByClass("result-game-item-info-tag").get(0).text();
            bookInfoBean.setAuthor(author);
            mSearchList.add(bookInfoBean);
        }

        return mSearchList;
    }
}
