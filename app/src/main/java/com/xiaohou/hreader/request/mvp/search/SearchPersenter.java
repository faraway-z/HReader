package com.xiaohou.hreader.request.mvp.search;

import com.blankj.utilcode.util.LogUtils;
import com.xiaohou.hreader.Constant;
import com.xiaohou.hreader.request.base.RequestSubscribe;
import com.xiaohou.hreader.request.base.RxLifeCycleUtils;
import com.xiaohou.hreader.request.base.RxThreadUtil;
import com.xiaohou.hreader.ui.base.BasePresenter;

public class SearchPersenter extends BasePresenter<SearchConcrat.view> implements SearchConcrat.persenter {
    private SearchMode mode;
    private int page = 1;
    public SearchPersenter() {
        mode = new SearchMode();
    }

    @Override
    public void onSearchData(String keyWord,boolean isLoad) {
        if (!isViewAttached()){
            return;
        }
        String url ;
        if (isLoad){
            page ++;
            url=Constant.SEARCH_URL+keyWord+"&page="+page;
        }else {
            page = 1;
            url = Constant.SEARCH_URL+keyWord;
        }
        mView.showLoading();
        mode.onSearchData(url)
                .compose(RxThreadUtil.rxObservableSchedulerHelper())
                .compose(RxLifeCycleUtils.bindToLifecycle(mView))
                .subscribe(new RequestSubscribe<String>() {
                    @Override
                    protected void onRequestSuccess(String response) {
                        mView.hideLoading();
                        mView.onSuccess(ParseSearchData.onSearchData(response),page);
                    }

                    @Override
                    protected void onRequestError(Object objectBean) {
                        mView.hideLoading();

                    }

                    @Override
                    protected void onNetWorkError() {
                        mView.hideLoading();

                    }
                });
    }
}
