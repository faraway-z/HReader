package com.xiaohou.hreader.request.mvp.details;

import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.request.bean.IndexCardBean;
import com.xiaohou.hreader.ui.base.BaseView;

import io.reactivex.Observable;

public interface BookDetailsConcrat {
    interface mode {
        Observable<String> getBookDetailsData(String path);
    }
    interface view extends BaseView{
        void onSuccess(IndexCardBean infoBean);
    }
    interface persenter{
        void getBookDetailsData(boolean isSearch,String path);
    }
}
