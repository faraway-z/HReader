package com.xiaohou.hreader.request.mvp.details;

import com.xiaohou.hreader.request.bean.BookChapterBean;
import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.request.bean.IndexCardBean;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * 解析书籍详情数据
 */
public class ParseDetailsDataUtils {
    private static final String TAG = "ParseDetailsDataUtils：";

    /**
     * 获取书籍详情
     *
     * @param response
     */
    public static IndexCardBean getBookDetailsData(String response) {
        Document mDetailsData = Jsoup.parse(response);
        IndexCardBean mBookInfo = new IndexCardBean();
        Element mBookInfoElement = mDetailsData.getElementById("maininfo");
        String bookName = mBookInfoElement.getElementsByTag("h1").text();
        String bookAuthor = mBookInfoElement.select("p").get(0).text();
        String bookTye = mBookInfoElement.select("p").get(1).text();
        String bookUpdateTime = mBookInfoElement.select("p").get(2).text();
        //获取最新章节
        String bookNewChapter = mBookInfoElement.select("p").get(3).text();
        mBookInfo.setName(bookName);
        mBookInfo.setAuthor(bookAuthor);
        mBookInfo.setType(bookTye.substring(0,bookTye.indexOf(",")));
        mBookInfo.setUpdateTime(bookUpdateTime);
        mBookInfo.setNewChapter(bookNewChapter);
        //获取简介
        Elements mBookDesElement = mDetailsData.getElementById("intro").getElementsByTag("p");
        StringBuilder mDescBuilder = new StringBuilder();
        for (Element p : mBookDesElement) {
            mDescBuilder.append(p.text());
        }
        mBookInfo.setInfo(mDescBuilder.toString());
        //获取书籍封面图
        String mImgHref = mDetailsData.getElementById("fmimg").select("img").attr("src");
        mBookInfo.setImgUrl(mImgHref);
        //获取推荐书籍
        Elements mRecommentBookList = mDetailsData.getElementById("listtj").select("a");
        List<BookInfoBean> mRecomment = new ArrayList<>();
        for (Element a : mRecommentBookList) {
            BookInfoBean bookInfoBean = new BookInfoBean();
            bookInfoBean.setName(a.text());
            bookInfoBean.setRoute(a.attr("href"));
            mRecomment.add(bookInfoBean);
        }
        mBookInfo.setRecommendInfo(mRecomment);
        //获取章节目录
        Elements mChapters = mDetailsData.getElementById("list").select("a");
        List<BookChapterBean> mChapterList = new ArrayList<>();
        for (Element chapter:mChapters){
            BookChapterBean bookInfoBean = new BookChapterBean();
            bookInfoBean.setUrl(chapter.attr("href"));
            bookInfoBean.setName(chapter.text());
            mChapterList.add(bookInfoBean);
        }
        mBookInfo.setChapters(mChapterList);
        return mBookInfo;
    }
}
