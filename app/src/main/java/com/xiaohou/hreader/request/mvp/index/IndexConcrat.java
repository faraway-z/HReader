package com.xiaohou.hreader.request.mvp.index;

import com.xiaohou.hreader.request.bean.IndexBean;
import com.xiaohou.hreader.ui.base.BaseView;

import io.reactivex.Observable;

/**
 * 书城首页
 */
public interface IndexConcrat {
    interface mode{
        Observable<String> getIndexData();
    }
    interface view extends BaseView{
        void onSuccess(IndexBean o);
    }
    interface persenter{
        void getIndexData();
    }
}
