package com.xiaohou.hreader.request.mvp.index;

import com.blankj.utilcode.util.LogUtils;
import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.request.bean.IndexBean;
import com.xiaohou.hreader.request.bean.IndexCardBean;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * 解析主页数据
 */
public class ParseIndexDataUtlis {

    private static final String TAG = "ParseIndexDataUtlis:  ";

    public static IndexBean getIndexData(String response) {
        IndexBean mIndexBean = new IndexBean();
        Document mIndexData = Jsoup.parse(response);
        List<BookInfoBean> mBannerList = getBannerListData(mIndexData);
        mIndexBean.setBannerList(mBannerList);
        List<IndexCardBean> mIndexCardListBean = getIndexCardListData(mIndexData);
        mIndexBean.setCardList(mIndexCardListBean);
        List<IndexCardBean> mNewCardListBean = getNewCardListData(mIndexData,1);
        mIndexBean.setNewBookList(mNewCardListBean);
        List<IndexCardBean> mNewCardListBean2 = getNewCardListData(mIndexData,0);
        mIndexBean.setNewStorageBookList(mNewCardListBean2);
        mIndexBean.setLoading(false);
        return mIndexBean;
    }

    /**
     * 获取最新数据数据
     *
     * @param mIndexData
     * @return
     */
    private static List<IndexCardBean> getNewCardListData(Document mIndexData,int type) {
        List<IndexCardBean> mList = new ArrayList<>();
        Elements mNewContentElements = mIndexData.getElementById("newscontent").getElementsByClass(type == 0?"r":"l");
        //获取最新书籍
        for (Element element : mNewContentElements) {
            IndexCardBean indexCardBean = new IndexCardBean();
            //获取卡片名称
            String name = element.getElementsByTag("h2").text();
            indexCardBean.setTypeName(name);
            Elements mUlElements = element.getElementsByTag("ul");
            for (Element ul : mUlElements) {
                List<BookInfoBean> mBookInfoList = new ArrayList<>();
                Elements mLiElements = ul.getElementsByTag("li");
                for (Element li : mLiElements) {
                    BookInfoBean bookInfoBean = new BookInfoBean();
                    String path = li.getElementsByClass("s2").select("a").attr("href");
                    String bookName = li.getElementsByClass("s2").select("a").text();
                    String author = li.getElementsByTag("span").select("span.s4").text();
                    bookInfoBean.setAuthor(author);
                    bookInfoBean.setName(bookName);
                    bookInfoBean.setRoute(path);
                    mBookInfoList.add(bookInfoBean);
                }
                indexCardBean.setRecommendInfo(mBookInfoList);
            }
            mList.add(indexCardBean);
        }
        return mList;
    }

    /**
     * 获取主页 Crad View Data
     *
     * @return
     */
    private static List<IndexCardBean> getIndexCardListData(Document mIndexData) {
        List<IndexCardBean> mList = new ArrayList<>();
        Elements mContentIElements = mIndexData.getElementsByClass("content");

        for (Element element : mContentIElements) {
            IndexCardBean bean = new IndexCardBean();
            //Item name
            String name = element.select("h2").text();
            bean.setTypeName(name);
            //书籍跳转路径
            String mBookPath = element.getElementsByClass("image").select("a").attr("href");
            bean.setRoute(mBookPath);
            //封面图
            String mBookImgUrl = element.getElementsByClass("image").select("img").attr("src");
            bean.setImgUrl(mBookImgUrl);
            //书名
            Elements elements = element.getElementsByClass("top").select("dl");
            String mBookName = elements.select("a").text();
            bean.setName(mBookName);
            //书籍说明
            String mBookDesc = element.select("dd").text();
            bean.setInfo(mBookDesc);
            //推荐数据
            List<BookInfoBean> mRecommendList = new ArrayList<>();
            Elements mRecommentBookTag = element.getElementsByTag("ul");
            for (Element mRecommentElement : mRecommentBookTag) {

                Elements mLiElements = mRecommentElement.getElementsByTag("li");
                for (Element li : mLiElements) {
                    BookInfoBean mRecommentBean = new BookInfoBean();
                    String path = li.select("a").attr("href");
                    mRecommentBean.setRoute(path);
                    String mmRecommentName = li.select("a").text();
                    mRecommentBean.setName(mmRecommentName);
                    mRecommendList.add(mRecommentBean);
                }
                bean.setRecommendInfo(mRecommendList);
            }
            mList.add(bean);
        }
        return mList;
    }

    /**
     * 获取Banner List 数据
     *
     * @param mIndexData
     * @return
     */
    private static List<BookInfoBean> getBannerListData(Document mIndexData) {
        List<BookInfoBean> mBannerList = new ArrayList<>();
        Elements mBannerElement = mIndexData.getElementsByClass("l");
        for (Element element : mBannerElement) {
            Elements mItem = element.getElementsByClass("image");
            for (int i = 0; i < mItem.size(); i++) {
                BookInfoBean infoBean = new BookInfoBean();
                String mHref = mItem.get(i).select("a").attr("href");
                String mImg = mItem.get(i).getElementsByTag("img").attr("src");
                infoBean.setRoute(mHref);
                infoBean.setImgUrl(mImg);
                mBannerList.add(infoBean);
            }
        }
        return mBannerList;
    }
}
