package com.xiaohou.hreader.request.mvp.details;

import com.xiaohou.hreader.request.base.RequestManager;

import io.reactivex.Observable;

public class BookDetailsMode implements BookDetailsConcrat.mode {
    @Override
    public Observable<String> getBookDetailsData(String path) {
        return RequestManager.getInstance().getApi.getBookDetailsData(path);
    }
}
