package com.xiaohou.hreader.request.bean;

import com.blankj.utilcode.util.LogUtils;

import org.litepal.LitePal;
import org.litepal.crud.LitePalSupport;
import org.litepal.crud.callback.SaveCallback;
import org.litepal.crud.callback.UpdateOrDeleteCallback;

import java.io.Serializable;
import java.util.List;

public class BookInfoBean extends LitePalSupport implements Serializable {
    public String name;
    public String info;
    public String updateTime;

    public BookInfoBean(boolean noData, String msg) {
        this.noData = noData;
        this.msg = msg;
    }

    public static void updateBookrack(IndexCardBean mIndexCardBean,UpdateOrDeleteCallback updateOrDeleteCallback) {
        BookInfoBean bookInfoBean = new BookInfoBean();
        bookInfoBean.setRoute(mIndexCardBean.getRoute());
        bookInfoBean.setAuthor(mIndexCardBean.getAuthor());
        bookInfoBean.setInfo(mIndexCardBean.getInfo());
        bookInfoBean.setImgUrl(mIndexCardBean.getImgUrl());
        bookInfoBean.updateAllAsync("name = ?",mIndexCardBean.getName()).listen(updateOrDeleteCallback);
    }

    public void setNoData(boolean noData) {
        this.noData = noData;
    }

    public boolean noData = false;
    public BookInfoBean() {
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String msg;

    /**
     * 查找书籍数据
     * @param name
     * @return
     */
    public static List<BookInfoBean> findBookInfo(String name) {
        return LitePal.where("name = ?",name).find(BookInfoBean.class);
    }

    /**
     * 删除书籍数据
     * @param name
     * @return
     */
    public static void deleteJoinBookrack(String name,UpdateOrDeleteCallback updateOrDeleteCallback) {
        LitePal.deleteAllAsync(BookInfoBean.class,"name = ?",name).listen(updateOrDeleteCallback);
    }

    /**
     * 保存书籍
     * @param mIndexCardBean
     * @return
     */
    public static void   saveBookrack(IndexCardBean mIndexCardBean,SaveCallback saveCallback) {
        BookInfoBean bookInfoBean = new BookInfoBean();
        bookInfoBean.setRoute(mIndexCardBean.getRoute());
        bookInfoBean.setName(mIndexCardBean.getName());
        bookInfoBean.setAuthor(mIndexCardBean.getAuthor());
        bookInfoBean.setInfo(mIndexCardBean.getInfo());
        bookInfoBean.setImgUrl(mIndexCardBean.getImgUrl());
        bookInfoBean.saveAsync().listen(saveCallback);
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String type;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String imgUrl;
    public String route;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "BookInfoBean{" +
                "name='" + name + '\'' +
                ", info='" + info + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", type='" + type + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", route='" + route + '\'' +
                ", mChapters=" + mChapters +
                ", author='" + author + '\'' +
                ", newChapter='" + newChapter + '\'' +
                '}';
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getNewChapter() {
        return newChapter;
    }

    public void setNewChapter(String newChapter) {
        this.newChapter = newChapter;
    }
    public List<BookChapterBean> mChapters ;
    public String author;

    public List<BookChapterBean> getChapters() {
        return mChapters;
    }

    public void setChapters(List<BookChapterBean> mChapters) {
        this.mChapters = mChapters;
    }

    public String newChapter;
}
