package com.xiaohou.hreader.request.bean;

import com.blankj.utilcode.util.LogUtils;

import org.litepal.LitePal;
import org.litepal.crud.LitePalSupport;
import org.litepal.crud.callback.SaveCallback;
import org.litepal.crud.callback.UpdateOrDeleteCallback;

import java.util.List;

public class BookReadBean extends LitePalSupport {
    private static final String TAG = "BookReadBean：";
    public String bookName;
    public String cacheKey;
    public int chapterIndex;

    /**
     * 查找阅读进度
     *
     * @param name
     */
    public static List<BookReadBean> onFindBookRead(String name) {
        return LitePal.where("bookName = ?", name).find(BookReadBean.class);
    }

    @Override
    public String toString() {
        return "BookReadBean{" +
                "bookName='" + bookName + '\'' +
                ", chapterIndex=" + chapterIndex +
                ", charIndex=" + charIndex +
                ", chapterTitle='" + chapterTitle + '\'' +
                '}';
    }

    public int charIndex;
    public String chapterTitle;

    /**
     * 保存阅读数据
     *
     * @param bookReadBean
     */
    public static void onSaveBookReading(BookReadBean bookReadBean) {
        List<BookReadBean> mList = LitePal.where("bookName = ?", bookReadBean.bookName).find(BookReadBean.class);
        if (mList != null && mList.size() != 0) {
            bookReadBean.updateAllAsync("bookName =?", bookReadBean.bookName).listen(new UpdateOrDeleteCallback() {
                @Override
                public void onFinish(int rowsAffected) {
                    LogUtils.e(TAG + "__更新数据__" + rowsAffected);
                }
            });
        } else {
            bookReadBean.saveAsync().listen(new SaveCallback() {
                @Override
                public void onFinish(boolean success) {
                    LogUtils.e(TAG + "__保存数据__" + success);
                }
            });
        }
    }


}
