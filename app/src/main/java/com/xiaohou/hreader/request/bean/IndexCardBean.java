package com.xiaohou.hreader.request.bean;

import java.util.List;

public class IndexCardBean extends BookInfoBean{
    public String typeName;

    public String getTypeName() {
        return typeName;
    }

    @Override
    public String toString() {
        return "IndexCardBean{" +
                "typeName='" + typeName + '\'' +
                ", recommendInfo=" + recommendInfo +
                ", name='" + name + '\'' +
                ", info='" + info + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", route='" + route + '\'' +
                ", author='" + author + '\'' +
                ", newChapter='" + newChapter + '\'' +
                '}';
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<BookInfoBean> recommendInfo;

    public List<BookInfoBean> getRecommendInfo() {
        return recommendInfo;
    }

    public void setRecommendInfo(List<BookInfoBean> recommendInfo) {
        this.recommendInfo = recommendInfo;
    }
}
