package com.xiaohou.hreader.request.mvp.index;

import com.xiaohou.hreader.request.base.RequestManager;

import io.reactivex.Observable;

public class IndexMode implements IndexConcrat.mode {
    @Override
    public Observable<String> getIndexData() {
        return RequestManager.getInstance().getApi.getIndexData();
    }
}
