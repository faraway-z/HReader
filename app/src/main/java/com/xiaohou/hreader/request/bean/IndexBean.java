package com.xiaohou.hreader.request.bean;

import java.util.List;

public class IndexBean {

    @Override
    public String toString() {
        return "IndexBean{" +
                "bannerList=" + bannerList +
                ", cardList=" + cardList +
                ", recommendList=" + recommendList +
                ", newBookList=" + newBookList +
                ", newStorageBookList=" + newStorageBookList +
                '}';
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public boolean isLoading = true;
    public List<BookInfoBean> getBannerList() {
        return bannerList;
    }

    public void setBannerList(List<BookInfoBean> bannerList) {
        this.bannerList = bannerList;
    }

    public List<IndexCardBean> getCardList() {
        return cardList;
    }

    public void setCardList(List<IndexCardBean> cardList) {
        this.cardList = cardList;
    }

    public List<BookInfoBean> getRecommendList() {
        return recommendList;
    }

    public void setRecommendList(List<BookInfoBean> recommendList) {
        this.recommendList = recommendList;
    }

    public List<IndexCardBean> getNewBookList() {
        return newBookList;
    }

    public void setNewBookList(List<IndexCardBean> newBookList) {
        this.newBookList = newBookList;
    }

    public List<IndexCardBean> getNewStorageBookList() {
        return newStorageBookList;
    }

    public void setNewStorageBookList(List<IndexCardBean> newStorageBookList) {
        this.newStorageBookList = newStorageBookList;
    }

    public List<BookInfoBean> bannerList;//Banner List


    public List<IndexCardBean> cardList;// 卡片
    public List<BookInfoBean> recommendList; //推荐
    public List<IndexCardBean> newBookList;//最新小说
    public List<IndexCardBean> newStorageBookList;//最新入库小说

}
