package com.xiaohou.hreader.request.mvp.search;

import com.blankj.utilcode.util.SPUtils;
import com.xiaohou.hreader.Constant;
import com.xiaohou.hreader.request.base.RequestManager;
import com.xiaohou.hreader.request.base.RequestManagers;

import io.reactivex.Observable;

public class SearchMode implements SearchConcrat.mode {
    @Override
    public Observable<String> onSearchData(String keyWord) {
        return RequestManagers.getInstance().getApi.onSearchData(keyWord);
    }
}
