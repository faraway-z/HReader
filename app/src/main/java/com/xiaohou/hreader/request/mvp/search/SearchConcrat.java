package com.xiaohou.hreader.request.mvp.search;

import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.ui.base.BaseView;

import java.util.List;

import io.reactivex.Observable;

public interface SearchConcrat {
    interface mode{
        Observable<String> onSearchData(String keyWord);
    }
    interface view extends BaseView{
        void onSuccess(List<BookInfoBean> mList, int page);
    }
    interface persenter{
        void onSearchData(String keyWord,boolean isLoad);
    }
}
