package com.xiaohou.hreader.request.mvp.reading;

import com.blankj.utilcode.util.LogUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ParseContentDataUtlis {
    public static String TAG = "ParseContentDataUtlis：";
    public static String SPACE = "       ";
    public static String getBookContent(String response) {
        Document document = Jsoup.parse(response);
        Elements mContentReadElements = document.getElementsByClass("content_read");
        for (Element contentRead:mContentReadElements){
            String content = contentRead.getElementById("content").text().replaceAll(" ","\n    ");
            LogUtils.e("获取解析数据："+content);
           return content;
        }
        return null;
    }

}
