package com.xiaohou.hreader.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.Gson;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xiaohou.hreader.Constant;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.request.bean.IndexCardBean;
import com.xiaohou.hreader.request.mvp.details.BookDetailsConcrat;
import com.xiaohou.hreader.request.mvp.details.BookDetailsPersenter;
import com.xiaohou.hreader.ui.adpter.RecommentAdapter;
import com.xiaohou.hreader.ui.base.BaseMVPActivity;
import com.xiaohou.hreader.utils.BlurTransformation;
import com.xiaohou.hreader.utils.GlideRoundTransform;

import org.litepal.crud.callback.SaveCallback;
import org.litepal.crud.callback.UpdateOrDeleteCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookDetailsActivity extends BaseMVPActivity<BookDetailsPersenter> implements BookDetailsConcrat.view {
    private static final float TOOL_BAR_HEIGHT = 400;//导航栏高度
    private static final String TAG = "BookDetailsActivity：";
    @BindView(R.id.navigation_bar_title_box)
    LinearLayout mNavigationBarTitleBox;
    @BindView(R.id.navigation_bar_search_box_ll)
    LinearLayout mNavigationBarSearchBoxLl;
    @BindView(R.id.book_details_book_desc)
    TextView mBookDetailsBookDesc;
    @BindView(R.id.book_details_img_bg)
    ImageView mBookDetailsImgBg;
    @BindView(R.id.book_details_img)
    ImageView mBookDetailsImg;
    @BindView(R.id.book_details_book_author)
    TextView mBookDetailsBookAuthor;
    @BindView(R.id.book_details_book_type)
    TextView mBookDetailsBookType;

    @BindView(R.id.book_details_collapsing_layout)
    CollapsingToolbarLayout mBookDetailsCollapsingLayout;
    @BindView(R.id.book_details_app_bar)
    AppBarLayout mBookDetailsAppBar;
    @BindView(R.id.navigation_bar_title)
    TextView mNavigationBarTitle;
    @BindView(R.id.navigation_bar_box)
    LinearLayout mNavigationBarBox;
    @BindView(R.id.book_details_smart_refresh_layout)
    SmartRefreshLayout mBookDetailsSmartRefreshLayout;
    @BindView(R.id.book_details_book_name)
    TextView mBookDetailsBookName;
    @BindView(R.id.book_details_book_update_time)
    TextView mBookDetailsBookUpdateTime;
    @BindView(R.id.book_details_book_recomment_recycler_view)
    RecyclerView mRecommentRecyclerView;
    @BindView(R.id.book_details_book_new_chapter)
    TextView mBookDetailsBookNewChapter;
    @BindView(R.id.book_details_join_check_box)
    CheckBox mBookDetailsJoinCheckBox;
    private float scale;
    private float alpha;
    private BookDetailsPersenter mBookDetailsPersenter;
    private List<BookInfoBean> mRecommentBookList = new ArrayList<>();
    private RecommentAdapter mRecommentAdapter;
    private IndexCardBean mIndexCardBean;

    public static void onIntentPager(Activity activity, String bookPath, boolean isSearch) {
        Intent intent = new Intent(activity, BookDetailsActivity.class);
        intent.putExtra(Constant.BOOK_PATH, bookPath);
        intent.putExtra(Constant.SEARCH_TYPE, isSearch);
        activity.startActivity(intent);
    }

    @Override
    protected void initView() {
        mBookDetailsSmartRefreshLayout.setEnableLoadMore(false);
        mBookDetailsSmartRefreshLayout.setOnRefreshListener(refreshLayout -> {
            refreshLayout.finishRefresh(2000);
            mBookDetailsPersenter.getBookDetailsData(BookDetailsActivity.this.getIntent().getBooleanExtra(Constant.SEARCH_TYPE, false), BookDetailsActivity.this.getIntent().getStringExtra(Constant.BOOK_PATH));
        });
        mBookDetailsPersenter.getBookDetailsData(getIntent().getBooleanExtra(Constant.SEARCH_TYPE, false), getIntent().getStringExtra(Constant.BOOK_PATH));
        mNavigationBarTitleBox.setVisibility(View.VISIBLE);
        mNavigationBarSearchBoxLl.setVisibility(View.GONE);
        mBookDetailsAppBar.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            int toolbarHeight = appBarLayout.getTotalScrollRange();
            int dy = Math.abs(verticalOffset);
            if (dy <= toolbarHeight) {
                scale = (float) dy / toolbarHeight;
                alpha = scale * 255;
            }
            if (alpha > ConvertUtils.px2dp(TOOL_BAR_HEIGHT)) {

                mNavigationBarBox.setBackgroundColor(BookDetailsActivity.this.getResources().getColor(SPUtils.getInstance().getBoolean(Constant.NIGHT_MDE_OPEN,false)?R.color.colorWhite_night:R.color.colorPrimary));
                if (mNavigationBarTitle != null) {
                    mNavigationBarTitle.setText(mBookDetailsBookName.getText().toString());
                }
            } else {
                mNavigationBarBox.setBackgroundColor(Color.TRANSPARENT);
                if (mNavigationBarTitle != null) {
                    mNavigationBarTitle.setText(BookDetailsActivity.this.getResources().getString(R.string.book_details));
                }
            }

        });
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(mContext);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        layoutManager.setAlignItems(AlignItems.STRETCH);
        mRecommentRecyclerView.setLayoutManager(layoutManager);
        mRecommentAdapter = new RecommentAdapter(R.layout.item_recomment, mRecommentBookList);
        mRecommentAdapter.setOnItemChildClickListener((adapter, view, position) -> BookDetailsActivity.onIntentPager(BookDetailsActivity.this, mRecommentBookList.get(position).getRoute(), false));
        mRecommentRecyclerView.setAdapter(mRecommentAdapter);
        mBookDetailsJoinCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> BookDetailsActivity.this.onJoinBookrack(isChecked));
    }


    @Override
    protected int getLayoutID() {
        return R.layout.activity_book_details;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.navigation_bar_title_back, R.id.book_details_book_new_chapter, R.id.book_details_reading_btn})
    public void onViewClicked(View view) {
        Gson gson = new Gson();

        switch (view.getId()) {
            case R.id.navigation_bar_title_back:
                finish();
                break;
            case R.id.book_details_reading_btn:
                if (mIndexCardBean == null) {
                    return;
                }
                BookReadingActivity.onIntentPager(BookDetailsActivity.this, 0, gson.toJson(mIndexCardBean), 0);
                break;
            case R.id.book_details_book_new_chapter:
                if (mIndexCardBean == null) {
                    return;
                }
                BookChapterActivity.onStartPager(this, gson.toJson(mIndexCardBean));
                break;
        }
    }

    /**
     * 加入书架
     *
     * @param isJoin
     */
    private void onJoinBookrack(boolean isJoin) {
        if (mIndexCardBean == null) {
            return;
        }
        if (!isJoin) {
            List<BookInfoBean> mBookInfoList = BookInfoBean.findBookInfo(mIndexCardBean.getName());
            if (mBookInfoList != null && mBookInfoList.size() != 0) {
                BookInfoBean.deleteJoinBookrack(mIndexCardBean.getName(), rowsAffected -> {
                });
            }
        } else {
            List<BookInfoBean> mBookInfoList = BookInfoBean.findBookInfo(mIndexCardBean.getName());
            if (mBookInfoList != null && mBookInfoList.size() != 0) {
                BookInfoBean.updateBookrack(mIndexCardBean, rowsAffected -> {
                });
            } else {
                BookInfoBean.saveBookrack(mIndexCardBean, success -> {
                });
            }
        }
    }

    /**
     * 设置加入书架 提示
     *
     * @param isSave
     */
    private void setJoinBookrackTag(boolean isSave) {
        if (mBookDetailsJoinCheckBox != null) {
            mBookDetailsJoinCheckBox.setChecked(isSave);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mIndexCardBean == null){
            return;
        }
        onAddBookrack(mIndexCardBean.getName());
    }

    @Override
    protected BookDetailsPersenter createPresenter() {
        mBookDetailsPersenter = new BookDetailsPersenter();
        return mBookDetailsPersenter;
    }

    @Override
    public void onSuccess(IndexCardBean infoBean) {
        mIndexCardBean = infoBean;
        if (mIndexCardBean == null) {
            return;
        }
        mIndexCardBean.setRoute(getIntent().getStringExtra(Constant.BOOK_PATH));
        onAddBookrack(mIndexCardBean.getName());
        Glide.with(mContext).load(mIndexCardBean.getImgUrl()).apply(new RequestOptions().transform(new GlideRoundTransform(5))).into(mBookDetailsImg);
        Glide.with(mContext).load(mIndexCardBean.getImgUrl()).apply(RequestOptions.bitmapTransform(new BlurTransformation(25, 8))).into(mBookDetailsImgBg);
        if (mBookDetailsBookName != null) {
            mBookDetailsBookName.setText(mIndexCardBean.getName());
        }
        if (mBookDetailsBookAuthor != null) {
            mBookDetailsBookAuthor.setText(mIndexCardBean.getAuthor());
        }
        if (mBookDetailsBookType != null) {
            mBookDetailsBookType.setText(mIndexCardBean.getType());
        }
        if (mBookDetailsBookUpdateTime != null) {
            mBookDetailsBookUpdateTime.setText(mIndexCardBean.getUpdateTime());
        }
        if (mBookDetailsBookDesc != null) {
            mBookDetailsBookDesc.setText(Html.fromHtml(mIndexCardBean.getInfo()));
        }
        String newChapterTag = mIndexCardBean.getNewChapter().substring(0, mIndexCardBean.getNewChapter().indexOf("：") + 1);
        String newChapter = mIndexCardBean.getNewChapter().substring(newChapterTag.length() + 1);
        SpannableString mNewChapterString = new SpannableString(newChapterTag + newChapter);
        mNewChapterString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorGray)), newChapterTag.length(), (newChapterTag + newChapter).length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        if (mBookDetailsBookNewChapter != null) {
            mBookDetailsBookNewChapter.setText(mNewChapterString);
        }
        mRecommentBookList.clear();
        mRecommentBookList.addAll(mIndexCardBean.getRecommendInfo());
        if (mRecommentAdapter != null) {
            mRecommentAdapter.setData(mRecommentBookList);
        }
    }

    /**
     * 添加到书架
     *
     * @param name
     */
    private void onAddBookrack(String name) {
        List<BookInfoBean> mBookrckList = BookInfoBean.findBookInfo(name);
        if (mBookrckList.size() != 0) {
            setJoinBookrackTag(true);
        } else {
            setJoinBookrackTag(false);
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onError(Object o) {

    }
}
