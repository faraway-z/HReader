package com.xiaohou.hreader.ui.adpter;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.request.bean.IndexCardBean;
import com.xiaohou.hreader.utils.GlideRoundTransform;

import java.util.List;

public class IndexOtherBookAdapter extends BaseQuickAdapter<IndexCardBean, BaseViewHolder> {
    public Context mContext;
    public List<IndexCardBean> data;
    private RecommentAdapter mRecommentAdapter;
    private OnItemClickListener onItemClickListener =null;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public void setData(List<IndexCardBean> data) {
        this.data = data;
        notifyDataSetChanged();

    }

    public IndexOtherBookAdapter(int layoutResId, @Nullable List<IndexCardBean> data, Context mContext) {
        super(layoutResId, data);
        this.mContext = mContext;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, IndexCardBean item) {
        helper.setText(R.id.item_index_card_info_type, item.getTypeName())
                .setText(R.id.item_index_card_info_desc, Html.fromHtml(item.getInfo()))
                .addOnClickListener(R.id.item_index_card_info)
                .setText(R.id.item_index_card_info_name, item.getName());
        Glide.with(mContext).load(item.getImgUrl()).apply(new RequestOptions().transform(new GlideRoundTransform(5))).into((ImageView) helper.getView(R.id.item_index_card_info_book_img));

        initRecommentView(helper, item);
    }

    /**
     * 推荐数据
     *
     * @param helper
     * @param item
     */
    private void initRecommentView(BaseViewHolder helper, final IndexCardBean item) {
        RecyclerView mRecommentRecyclerView = helper.getView(R.id.item_index_card_info_recycler_view);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(mContext);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        layoutManager.setAlignItems(AlignItems.STRETCH);
        mRecommentRecyclerView.setLayoutManager(layoutManager);
        mRecommentAdapter = new RecommentAdapter(R.layout.item_recomment, item.getRecommendInfo());
        mRecommentAdapter.setOnItemChildClickListener(new OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(item.getRecommendInfo().get(position));
                }
            }
        });
        mRecommentRecyclerView.setAdapter(mRecommentAdapter);
    }

    /**
     * Item 点击事件
     */
    public interface OnItemClickListener {
        /**
         * 点击事件
         *
         * @param bean
         */
        void onItemClick(BookInfoBean bean);
    }
}
