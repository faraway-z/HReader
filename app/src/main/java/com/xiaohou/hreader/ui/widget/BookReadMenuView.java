package com.xiaohou.hreader.ui.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.SPUtils;
import com.bumptech.glide.Glide;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.xiaohou.hreader.Constant;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.ui.base.event.EventBusUtlis;
import com.xiaohou.hreader.ui.widget.reader.widget.EffectOfCover;
import com.xiaohou.hreader.ui.widget.reader.widget.EffectOfNon;
import com.xiaohou.hreader.ui.widget.reader.widget.EffectOfRealBothWay;
import com.xiaohou.hreader.ui.widget.reader.widget.EffectOfRealOneWay;
import com.xiaohou.hreader.ui.widget.reader.widget.EffectOfSlide;
import com.xiaohou.hreader.utils.SystemBarUtils;

public class BookReadMenuView extends RelativeLayout implements SeekBar.OnSeekBarChangeListener, View.OnClickListener {

    private static final String TAG = "BookReadMenuView";
    private boolean isShowing;
    private static final int ANIMATION_DURATION = 200;

    private LinearLayout mToolbar;
    private RelativeLayout mBottomMenu;
    private TextView mBookNameTextView;
    private TextView mBookChapterNameTextView;
    private TextView mBookChapterProgressNameTextView;
    private SeekBar mBookSeekBarProgress;
    private LinearLayout mTypefaceSeekBarBox;
    private CheckBox mBookModeChangeBox;


    private RadioGroup mPageModeBox;
    private RadioGroup mPageColorBox;
    private RadioButton mBookPagerModeDefault;
    private RadioButton mBookPagerModeCover;
    private RadioButton mBookPagerModeEmulation;
    private RadioButton mBookPagerModeNothing;
    private RadioButton mBookPagerModeslide;
    private RadioButton mBookPagerColorWhite;
    private RadioButton mBookPagerColorOne;
    private RadioButton mBookPagerColorTwo;
    private RadioButton mBookPagerColorThree;
    private RadioButton mBookPagerColorFour;

    /**
     * 设置章节进度
     *
     * @param chapterProgress
     */
    public void setBookChapterProgress(String chapterProgress) {
        if (chapterProgress.equals("")) {
            return;
        }
        if (mBookChapterProgressNameTextView != null) {
            mBookChapterProgressNameTextView.setText(chapterProgress);
        }
    }

    public void setChapterCallBack(OnChapterCallBack mChapterCallBack) {
        this.mChapterCallBack = mChapterCallBack;
    }

    private Activity mActivity;

    public void onAddBookrack(boolean isChecked) {
        if (mBookAddBookrack != null) {
            mBookAddBookrack.setChecked(isChecked);
        }
        if (mBookAddBookrack != null) {
            mBookAddBookrack.setText(getResources().getString(isChecked == true ? R.string.jion_bookrack_s : R.string.jion_bookrack));
        }
    }

    public void onShowReadBg(boolean isShow) {
        if (mPageColorBox != null) {
            mPageColorBox.setVisibility(isShow == true ? GONE : VISIBLE);
        }
    }

    private CheckBox mBookAddBookrack;
    private OnChapterCallBack mChapterCallBack = null;

    public SeekBar getBookSeekBarTypefaceProgress() {
        return mBookSeekBarTypefaceProgress;
    }

    private SeekBar mBookSeekBarTypefaceProgress;

    public SeekBar getBookSeekBarProgress() {
        return mBookSeekBarProgress;
    }

    public CheckBox getModeChangeBox() {
        return mBookModeChangeBox;
    }

    public CheckBox getAddBookrack() {
        return mBookAddBookrack;
    }

    public void setCallBack(OnMenuCallBack callBack) {
        this.callBack = callBack;
    }

    private OnMenuCallBack callBack = null;

    public BookReadMenuView(@NonNull Context context) {
        this(context, null);
    }

    public BookReadMenuView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public void setActivity(Activity mActivity) {
        this.mActivity = mActivity;
    }

    public BookReadMenuView(@NonNull final Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.layout_book_reading_menu, this);
        mToolbar = findViewById(R.id.book_reading_title_bar_box);
        mBottomMenu = findViewById(R.id.book_reading_bottom_box);
        mBookNameTextView = findViewById(R.id.title_bar_title_reading);
        mBookModeChangeBox = findViewById(R.id.book_reading_mode_change);
        mBookPagerModeDefault = findViewById(R.id.change_pager_mde_default);
        mBookPagerModeCover = findViewById(R.id.change_pager_mde_cover);
        mBookPagerModeEmulation = findViewById(R.id.change_pager_mde_emulation);
        mBookPagerModeNothing = findViewById(R.id.change_pager_mde_nothing);
        mBookPagerColorWhite = findViewById(R.id.change_pager_color_white);
        mBookPagerColorOne = findViewById(R.id.change_pager_color_one);
        mBookPagerColorTwo = findViewById(R.id.change_pager_color_two);
        mBookPagerColorThree = findViewById(R.id.change_pager_color_three);
        mBookPagerColorFour = findViewById(R.id.change_pager_color_four);
        mPageModeBox = findViewById(R.id.change_pager_mde_box);
        mPageColorBox = findViewById(R.id.change_pager_color_box);
        mPageModeBox.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (callBack != null) {
                    callBack.onRadioChanged(checkedId);
                }
            }
        });
        mPageColorBox.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (callBack != null) {
                    callBack.onRadioChanged(checkedId);
                }
            }
        });
        mBookAddBookrack = findViewById(R.id.book_reading_add_bookrack);
        mBookAddBookrack.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (callBack != null) {
                    callBack.onCheckedChanged(R.id.book_reading_add_bookrack, b);
                }
            }
        });
        mBookModeChangeBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (callBack != null) {
                    callBack.onCheckedChanged(R.id.book_reading_mode_change, b);
                }
            }
        });
        findViewById(R.id.book_reading_setting_tv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTypefaceSeekBarBox.getVisibility() == View.VISIBLE) {
                    mTypefaceSeekBarBox.setVisibility(GONE);

                } else {
                    mTypefaceSeekBarBox.setVisibility(VISIBLE);
                }
            }
        });
        findViewById(R.id.read_chapter_last_chapter).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mChapterCallBack != null) {
                    mChapterCallBack.onLastClick();
                }
            }
        });
        findViewById(R.id.read_chapter_next_chapter).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mChapterCallBack != null) {
                    mChapterCallBack.onNextClick();
                }
            }
        });
        findViewById(R.id.book_reading_title_bar_ll).setOnClickListener(this);
        findViewById(R.id.read_chapter_minus_typeface).setOnClickListener(this);
        findViewById(R.id.read_chapter_add_typeface).setOnClickListener(this);
        findViewById(R.id.book_reading_catalogue_tv).setOnClickListener(this);
        mBookChapterNameTextView = findViewById(R.id.read_chapter_name_tv);
        mBookChapterProgressNameTextView = findViewById(R.id.read_chapter_progress_tv);
        mBookSeekBarProgress = findViewById(R.id.read_sb_chapter_progress);
        mBookSeekBarTypefaceProgress = findViewById(R.id.read_sb_typeface_progress);
        mTypefaceSeekBarBox = findViewById(R.id.read_sb_typeface_box);
        mBookSeekBarTypefaceProgress.setOnSeekBarChangeListener(this);
        mBookSeekBarProgress.setOnSeekBarChangeListener(this);
        mBookSeekBarTypefaceProgress.setMax(100);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ((Activity) context).getWindow().getDecorView().addOnLayoutChangeListener(new OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    ((Activity) context).getWindow().getDecorView().removeOnLayoutChangeListener(this);
                    mToolbar.setTranslationY(-mToolbar.getHeight());
                    mBottomMenu.setTranslationY(mBottomMenu.getHeight());
                    setVisibility(GONE);
                }
            });
        }
        if (mBookModeChangeBox != null) {
            mBookModeChangeBox.setChecked(SPUtils.getInstance().getBoolean(Constant.NIGHT_MDE_OPEN, false));
        }
        setClickable(true);
        initPageMode();
        initPageBg();
    }

    public void initPageBg() {
        switch (SPUtils.getInstance().getString(Constant.READ_BG, Constant.READ_BG_4)) {
            case Constant.READ_BG_0:
                if (mBookPagerColorOne != null) {
                    mBookPagerColorOne.setChecked(true);
                }
                break;
            case Constant.READ_BG_1:
                if (mBookPagerColorTwo != null) {
                    mBookPagerColorTwo.setChecked(true);
                }
                break;
            case Constant.READ_BG_2:
                if (mBookPagerColorThree != null) {
                    mBookPagerColorThree.setChecked(true);
                }
                break;
            case Constant.READ_BG_3:
                if (mBookPagerColorFour != null) {
                    mBookPagerColorFour.setChecked(true);
                }
                break;
            default:
                if (mBookPagerColorWhite != null) {
                    mBookPagerColorWhite.setChecked(true);
                }
                break;
        }
    }

    /**
     * 初始化 翻页
     */
    private void initPageMode() {
        switch (SPUtils.getInstance().getInt(Constant.EFFECT_TYPE, Constant.DEFAULT_PAGE)) {
            case Constant.EFFECT_OF_COVER_PAGE:
                if (mBookPagerModeCover != null) {
                    mBookPagerModeCover.setChecked(true);
                }
                break;
            case Constant.NOTHING_PAGE:
                //无
                if (mBookPagerModeNothing != null) {
                    mBookPagerModeNothing.setChecked(true);
                }
                break;
            case Constant.SLIDE_PAGE:
                //滑动
                if (mBookPagerModeslide != null) {
                    mBookPagerModeslide.setChecked(true);
                }
                break;
            case Constant.EMULATION_PAGE:

                //仿真
                if (mBookPagerModeEmulation != null) {
                    mBookPagerModeEmulation.setChecked(true);
                }
                break;
            default:
                if (mBookPagerModeDefault != null) {
                    mBookPagerModeDefault.setChecked(true);
                }
                break;
        }
    }


    private AnimatorSet mShowAnim;

    public void show() {
        setVisibility(View.VISIBLE);
        post(() -> BookReadMenuView.this.showAnim());
    }

    /**
     * 设置最大进度
     *
     * @param size
     * @param pageIndex
     */
    public void setSeekBarMaxProgress(int size, int pageIndex) {
        if (mBookSeekBarProgress != null) {
            mBookSeekBarProgress.setMax(Math.max(0, size - 1));
            mBookSeekBarProgress.setProgress(pageIndex);
        }
    }

    public void setSeekBarProgress(int progress) {
        if (mBookSeekBarProgress != null) {
            mBookSeekBarProgress.setProgress(progress);
        }
    }

    public void setSeekBarTypefaceProgress(int progress) {
        if (mBookSeekBarTypefaceProgress != null) {
            mBookSeekBarTypefaceProgress.setProgress(progress);
        }
    }

    /**
     * 设置书名
     *
     * @param name
     */
    public void setBookName(String name) {
        if (name == null || name.equals("")) {
            return;
        }
        if (mBookNameTextView != null) {
            mBookNameTextView.setText(name);
        }
    }

    /**
     * 设置章节名称
     *
     * @param chapterName
     */
    public void setBookChapterName(String chapterName) {
        if (chapterName == null || chapterName.equals("")) {
            return;
        }
        if (mBookChapterNameTextView != null) {
            mBookChapterNameTextView.setText(chapterName);
        }
    }

    private void showAnim() {
        if (mShowAnim == null) {
            ObjectAnimator toolbarAnim = null;
            toolbarAnim = ObjectAnimator.ofFloat(mToolbar, "translationY", -mToolbar.getHeight(), 0);
            ObjectAnimator bottomMenuAnim = ObjectAnimator.ofFloat(mBottomMenu, "translationY", mBottomMenu.getHeight(), 0);
            mShowAnim = new AnimatorSet();
            mShowAnim.play(toolbarAnim).with(bottomMenuAnim);
            mShowAnim.setDuration(ANIMATION_DURATION);
            mShowAnim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    isShowing = true;
                    showSystemBar(true);
                }
            });

        }
        if (!mShowAnim.isRunning()) {
            mShowAnim.start();
        }


    }

    private AnimatorSet mDismissAnim;

    public void dismiss() {
        if (mDismissAnim == null) {
            ObjectAnimator toolbarAnim = null;
            toolbarAnim = ObjectAnimator.ofFloat(mToolbar, "translationY", -mToolbar.getHeight());
            ObjectAnimator bottomMenuAnim = ObjectAnimator.ofFloat(mBottomMenu, "translationY", mBottomMenu.getHeight());
            mDismissAnim = new AnimatorSet();
            mDismissAnim.play(toolbarAnim).with(bottomMenuAnim);
            mDismissAnim.setDuration(ANIMATION_DURATION);
            mDismissAnim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    showSystemBar(false);
                    mTypefaceSeekBarBox.setVisibility(GONE);
                    setVisibility(View.GONE);
                    isShowing = false;
                }
            });


        }
        if (!mDismissAnim.isRunning()) {
            mDismissAnim.start();
        }


    }

    /**
     * 展示状态栏
     *
     * @param isShow
     */
    private void showSystemBar(boolean isShow) {
        if (isShow) {
            QMUIStatusBarHelper.setStatusBarDarkMode(mActivity);
            SystemBarUtils.showUnStableStatusBar(mActivity);
        } else {
            QMUIStatusBarHelper.setStatusBarLightMode(mActivity);
            SystemBarUtils.hideStableStatusBar(mActivity);
        }
    }

    public boolean isShowing() {
        return isShowing;
    }

    private boolean tempShowing;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                tempShowing = isShowing;
                break;
            case MotionEvent.ACTION_UP:
                if (tempShowing && isShowing)
                    dismiss();
                break;
        }
        return super.onTouchEvent(event);
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (callBack != null) {
            callBack.onProgressChanged(seekBar, progress, fromUser);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View view) {
        if (callBack != null) {
            callBack.onClick(view);
        }
    }

    public interface OnMenuCallBack {
        void onClick(View view);

        void onRadioChanged(int checkedId);

        void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser);

        void onCheckedChanged(int checkedId, boolean isChecked);
    }

    public interface OnChapterCallBack {
        void onNextClick();

        void onLastClick();

    }
}
