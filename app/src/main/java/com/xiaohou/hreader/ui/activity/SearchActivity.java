package com.xiaohou.hreader.ui.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.xiaohou.hreader.Constant;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.request.mvp.search.SearchConcrat;
import com.xiaohou.hreader.request.mvp.search.SearchPersenter;
import com.xiaohou.hreader.ui.adpter.BookrackAdapter;
import com.xiaohou.hreader.ui.base.BaseMVPActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends BaseMVPActivity<SearchPersenter> implements SearchConcrat.view, OnRefreshLoadMoreListener {

    @BindView(R.id.search_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.search_et_input)
    EditText mSearchEtInput;
    @BindView(R.id.search_smart_refresh)
    SmartRefreshLayout mSmartRefresh;
    private SearchPersenter mSearchPersenter;
    private BookrackAdapter mBookrackAdapter;
    private List<BookInfoBean> mList = new ArrayList<>();
    private RefreshLayout loadRefreshLayout;
    private RefreshLayout mRefreshLayout;
    private QMUITipDialog qmuiTipDialog;
    private View mEmptyView;

    @Override
    protected void initView() {
        showLoadingView();
        mSmartRefresh.setOnRefreshLoadMoreListener(this);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        mBookrackAdapter = new BookrackAdapter(R.layout.item_bookrack, mList);
        mBookrackAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                BookDetailsActivity.onIntentPager(SearchActivity.this, mList.get(position).getRoute(), true);
            }
        });
        mBookrackAdapter.setEmptyView(getEmptyView());
        mRecyclerView.setAdapter(mBookrackAdapter);
        mSearchEtInput.addTextChangedListener(new TextWatcher() {
            private CharSequence temp;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                temp = charSequence;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (temp.length() == 0) {
                    mList.clear();
                    if (mBookrackAdapter != null) {
                        mBookrackAdapter.setData(mList);
                    }
                }
            }
        });
        mSearchEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    SearchActivity.this.onGetSearchData(false);
                }
                return false;
            }
        });
    }

    /**
     * 占位图
     * @return
     */
    private View getEmptyView() {
        mEmptyView = LayoutInflater.from(mContext).inflate(R.layout.layout_empty_view,null);
        TextView mMsg = mEmptyView.findViewById(R.id.empty_view_msg);
        mMsg.setText(getResources().getString(R.string.no_search_msg));
        ImageView mImageView = mEmptyView.findViewById(R.id.empty_view_img);
        Glide.with(mContext).load(R.mipmap.ic_saerch_empty).into(mImageView);
        return mEmptyView;
    }

    private void showLoadingView() {
        qmuiTipDialog = new QMUITipDialog.Builder(mContext).setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING).setTipWord(getResources().getString(R.string.loading)).create();
        qmuiTipDialog.setCancelable(false);
        qmuiTipDialog.setCanceledOnTouchOutside(false);
    }

    /**
     * 获取搜索数据
     *
     * @param isLoad
     */
    private void onGetSearchData(boolean isLoad) {
        String searchKeyWord = mSearchEtInput.getText().toString();
        if (searchKeyWord.equals("")) {
            mList.clear();
            if (mBookrackAdapter != null) {
                mBookrackAdapter.setData(mList);
            }
            return;
        }
        mSearchPersenter.onSearchData(searchKeyWord, isLoad);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_search;
    }

    @Override
    protected SearchPersenter createPresenter() {
        mSearchPersenter = new SearchPersenter();
        return mSearchPersenter;
    }

    @Override
    public void onSuccess(List<BookInfoBean> data, int page) {
        if (page == 1) {
            mList.clear();
        }
        if (data.size() != 0) {
            mList.addAll(data);
        } else {
            if (page != 1) {
                ToastUtils.showShort(getResources().getString(R.string.no_load_data));
            }
        }
        if (mBookrackAdapter != null) {
            mBookrackAdapter.setData(mList);
        }


    }

    @Override
    public void showLoading() {
        if (qmuiTipDialog != null) {
            if (!qmuiTipDialog.isShowing()) {
                qmuiTipDialog.show();
            }
        }
    }

    @Override
    public void hideLoading() {
        if (mRefreshLayout != null) {
            mRefreshLayout.finishRefresh();
        }
        if (loadRefreshLayout != null) {
            loadRefreshLayout.finishLoadMore();
        }
        if (qmuiTipDialog != null) {
            qmuiTipDialog.dismiss();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        SPUtils.getInstance().put(Constant.CHARSET, Constant.GBK);
    }

    @Override
    public void onError(Object o) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        loadRefreshLayout = refreshLayout;
        onGetSearchData(true);
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        mRefreshLayout = refreshLayout;
        onGetSearchData(false);

    }

    @OnClick(R.id.navigation_bar_title_back)
    public void onViewClicked() {
        finish();
    }
}
