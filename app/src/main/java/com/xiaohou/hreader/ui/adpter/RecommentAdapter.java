package com.xiaohou.hreader.ui.adpter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.request.bean.BookInfoBean;

import java.util.List;

public class RecommentAdapter extends BaseQuickAdapter<BookInfoBean, BaseViewHolder> {
    public List<BookInfoBean> data;

    public void setData(List<BookInfoBean> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public RecommentAdapter(int layoutResId, @Nullable List<BookInfoBean> data) {
        super(layoutResId, data);
        this.data =data;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, BookInfoBean item) {
        helper.setText(R.id.item_recomment_desc,item.getName()).addOnClickListener(R.id.item_recomment_box);
    }
}
