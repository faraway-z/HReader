package com.xiaohou.hreader.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.QMUITabSegment;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.CrashReport;
import com.xiaohou.hreader.Constant;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.ui.adpter.FragmentAdapter;
import com.xiaohou.hreader.ui.base.BaseActivity;
import com.xiaohou.hreader.ui.fragment.BookrackFragment;
import com.xiaohou.hreader.ui.fragment.IndexFragment;
import com.xiaohou.hreader.ui.widget.NoSlidingViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IndexActivity extends BaseActivity {
    @BindView(R.id.index_view_pager)
    NoSlidingViewPager mViewPager;
    @BindView(R.id.index_qmui_tab_segment)
    QMUITabSegment mQmuiTabSegment;
    private List<Fragment> mFragments = new ArrayList<>();
    private FragmentAdapter mFragmentAdapter;
    private QMUITabSegment.Tab mTabIndex;
    private QMUITabSegment.Tab mTabClassify;
    @BindView(R.id.navigation_bar_search_box_ll)
    LinearLayout mNavigationBarSearchBox;
    @BindView(R.id.navigation_bar_title_box)
    LinearLayout mNavigationBarBox;

    @Override
    protected void initView() {
        mViewPager.setScanScroll(false);
        mNavigationBarSearchBox.setVisibility(View.VISIBLE);
        mNavigationBarBox.setVisibility(View.GONE);
        mFragments.clear();
        mFragments.add(IndexFragment.newInstance("书城"));
        mFragments.add(BookrackFragment.newInstance("书架"));
        mFragmentAdapter = new FragmentAdapter(getSupportFragmentManager(), mFragments);
        mFragmentAdapter.setList(mFragments);
        mViewPager.setAdapter(mFragmentAdapter);
        mQmuiTabSegment.setHasIndicator(false);
        mQmuiTabSegment.setMode(QMUITabSegment.MODE_FIXED);
        mQmuiTabSegment.setTabTextSize(QMUIDisplayHelper.sp2px(mContext, 13));
        mQmuiTabSegment.setDefaultNormalColor(getResources().getColor(R.color.colorGray));
        mQmuiTabSegment.setDefaultSelectedColor(getResources().getColor(R.color.colorPink));
        mQmuiTabSegment.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        mTabIndex = new QMUITabSegment.Tab(getResources().getDrawable(R.mipmap.ic_explore_black_24dp), getResources().getDrawable(R.mipmap.ic_explore_black_24dp), getResources().getString(R.string.book_mall), true, true);
        mTabClassify = new QMUITabSegment.Tab(getResources().getDrawable(R.mipmap.ic_bookrack_24dp), getResources().getDrawable(R.mipmap.ic_bookrack_24dp), getResources().getString(R.string.bookrack), true, true);
        mQmuiTabSegment.addTab(mTabIndex);
        mQmuiTabSegment.addTab(mTabClassify);
        mViewPager.setOffscreenPageLimit(mFragments.size() - 1);
        mQmuiTabSegment.setupWithViewPager(mViewPager, false);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_index;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.navigation_bar_search_box_ll, R.id.navigation_bar_setting_box})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.navigation_bar_search_box_ll:
                startActivity(new Intent(IndexActivity.this, SearchActivity.class));
                break;
            case R.id.navigation_bar_setting_box:
                startActivity(new Intent(IndexActivity.this, SettingActivity.class));
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mQmuiTabSegment != null) {
            mQmuiTabSegment.setBackgroundColor(SPUtils.getInstance().getBoolean(Constant.NIGHT_MDE_OPEN, false) ? getResources().getColor(R.color.colorWhite_night) : getResources().getColor(R.color.colorWhite));
        }

    }
}
