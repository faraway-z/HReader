package com.xiaohou.hreader.ui.adpter;

import com.chad.library.adapter.base.entity.MultiItemEntity;

public class ItemMultiItemEntity implements MultiItemEntity {
    public static final int INDEX_MULTI_ITEM_BANNER = 0;//Index Banner
    public static final int INDEX_MULTI_ITEM_CARD = 1;//Index Card
    public static final int INDEX_MULTI_ITEM_NEW = 2;//最新书籍
    public static final int INDEX_MULTI_ITEM_NEW_JION = 3;//最新书籍
    public static final int INDEX_MULTI_ITEM_NO_DATA = 4;//没有更多数据啦
    private int type;

    public ItemMultiItemEntity(int type) {
        this.type = type;
    }

    @Override
    public int getItemType() {
        return type;
    }
}
