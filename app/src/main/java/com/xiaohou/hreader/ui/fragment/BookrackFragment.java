package com.xiaohou.hreader.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.ui.activity.BookDetailsActivity;
import com.xiaohou.hreader.ui.adpter.BookrackAdapter;
import com.xiaohou.hreader.ui.base.BaseFragment;

import org.litepal.LitePal;
import org.litepal.crud.callback.UpdateOrDeleteCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class BookrackFragment extends BaseFragment {

    @BindView(R.id.bookrack_recycler_view)
    RecyclerView mRecyclerView;
    private List<BookInfoBean> mList = new ArrayList<>();
    private BookrackAdapter mBookrackAdapter;

    @Override
    protected void lazyLoadShow() {
       onFindBookrackData();
    }

    private void onFindBookrackData() {
        List<BookInfoBean> mBookInfoList = LitePal.findAll(BookInfoBean.class);
        mList.clear();
        if (mBookInfoList != null && mBookInfoList.size() != 0) {
            mList.addAll(mBookInfoList);
        }
        if (mBookrackAdapter != null) {
            mBookrackAdapter.setData(mList);
        }
    }

    public static BookrackFragment newInstance(String typeName) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(TYPE_NAME, typeName);
        BookrackFragment fragment = new BookrackFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void lazyLoadHide() {

    }

    @Override
    protected void initView() {
        super.initView();
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        mBookrackAdapter = new BookrackAdapter(R.layout.item_bookrack, mList);
        mBookrackAdapter.setOnItemChildLongClickListener(new BaseQuickAdapter.OnItemChildLongClickListener() {
            @Override
            public boolean onItemChildLongClick(BaseQuickAdapter adapter, View view, int position) {
                mBookrackAdapter.setDeleteShow(true);
                return false;
            }
        });
        mBookrackAdapter.setEmptyView(getEmptyView());
        mBookrackAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.item_bookrack_box:
                        BookDetailsActivity.onIntentPager(BookrackFragment.this.getActivity(), mList.get(position).getRoute(), false);
                        break;
                    case R.id.item_bookrack_delete_btn:
                        BookrackFragment.this.onRemoveBookrack(mList.get(position));
                        break;
                }
            }
        });
        mRecyclerView.setAdapter(mBookrackAdapter);
    }

    private View getEmptyView() {
        View mEmptyView = LayoutInflater.from(mContext).inflate(R.layout.layout_empty_view,null);
        return mEmptyView;
    }

    private void onRemoveBookrack(final BookInfoBean bookInfoBean) {
        if (bookInfoBean == null) {
            return;
        }
        mBookrackAdapter.setDeleteShow(false);
        BookInfoBean.deleteJoinBookrack(bookInfoBean.getName(), new UpdateOrDeleteCallback() {
            @Override
            public void onFinish(int rowsAffected) {
                if (rowsAffected == 0) {
                    ToastUtils.showShort(BookrackFragment.this.getResources().getString(R.string.remove_error));
                    return;
                }
                if (mList.size() == 0) {
                    mBookrackAdapter.setDeleteShow(false);
                } else {
                    mBookrackAdapter.setDeleteShow(true);

                }
                mList.remove(bookInfoBean);
                mBookrackAdapter.setData(mList);

            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_bookrack;
    }

    @Override
    public void onResume() {
        super.onResume();
        onFindBookrackData();
    }
}
