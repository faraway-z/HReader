package com.xiaohou.hreader.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.xiaohou.hreader.Constant;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.request.bean.BookChapterBean;
import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.request.bean.BookReadBean;
import com.xiaohou.hreader.request.bean.IndexCardBean;
import com.xiaohou.hreader.ui.adpter.BookContentAdapter;
import com.xiaohou.hreader.ui.adpter.ChapterAdapter;
import com.xiaohou.hreader.ui.base.BaseActivity;
import com.xiaohou.hreader.ui.widget.BookReadMenuView;
import com.xiaohou.hreader.ui.widget.reader.widget.EffectOfCover;
import com.xiaohou.hreader.ui.widget.reader.widget.EffectOfNon;
import com.xiaohou.hreader.ui.widget.reader.widget.EffectOfRealBothWay;
import com.xiaohou.hreader.ui.widget.reader.widget.EffectOfRealOneWay;
import com.xiaohou.hreader.ui.widget.reader.widget.EffectOfSlide;
import com.xiaohou.hreader.ui.widget.reader.widget.OnReaderWatcherListener;
import com.xiaohou.hreader.ui.widget.reader.widget.ReaderResolve;
import com.xiaohou.hreader.ui.widget.reader.widget.ReaderView;
import com.xiaohou.hreader.utils.ColorsConfig;
import com.xiaohou.hreader.utils.SystemBarUtils;

import org.litepal.LitePal;
import org.litepal.crud.callback.SaveCallback;
import org.litepal.crud.callback.UpdateOrDeleteCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import skin.support.SkinCompatManager;

public class BookReadingActivity extends BaseActivity implements OnReaderWatcherListener, BookReadMenuView.OnChapterCallBack, BookReadMenuView.OnMenuCallBack {
    private static final String TAG = "BookReadingActivity：";
    @BindView(R.id.book_read_view)
    ReaderView mBookReadView;
    @BindView(R.id.book_read_menu_view)
    BookReadMenuView mBookReadMenuView;
    @BindView(R.id.read_chapter_list)
    RecyclerView mReadChapterList;
    @BindView(R.id.read_ll_left)
    LinearLayout mReadLlLeft;
    @BindView(R.id.read_drawer)
    DrawerLayout mReadDrawer;
    private ReaderView.ReaderManager mReaderManager;
    private BookContentAdapter mAdapter;
    private IndexCardBean mIndexCardBean;
    private long mDownTime;
    private float mDownX;
    private static final int READ_CHANGE_PAGE = 0;//阅读页面改变
    private static final int READ_CHANGE_CHAPTER = 1;//章节改变
    private boolean isDrawerOpen = false;
    private List<BookChapterBean> mBookChapterList = new ArrayList<>();
    private ChapterAdapter mCatalogAdapter;
    private AlertDialog.Builder mHistoryDialogBuilder;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int longClickTime = ViewConfiguration.get(this).getLongPressTimeout();
        int touchSlop = ViewConfiguration.get(this).getScaledPagingTouchSlop();
        int screenWidth = ScreenUtils.getScreenWidth();
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (ev.getX() > screenWidth / 3 && ev.getX() < screenWidth * 2 / 3) {
                    mDownTime = System.currentTimeMillis();
                } else {
                    mDownTime = 0;
                }
                mDownX = ev.getX();
                break;
            case MotionEvent.ACTION_UP:
                if (System.currentTimeMillis() - mDownTime < longClickTime && (Math.abs(ev.getX() - mDownX) < touchSlop)) {
                    if (!isDrawerOpen) {
                        if (!mBookReadMenuView.isShowing()) {
                            mBookReadMenuView.show();
                            return true;
                        }
                    }
                }
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {


        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case READ_CHANGE_PAGE:
                    int pageIndex = (int) msg.obj;
                    int index = pageIndex + 1;
                    if (mBookReadMenuView != null) {
                        if (!mBookReadMenuView.isShowing()) {
                            mBookReadMenuView.setBookChapterProgress(index + "/" + mReaderManager.getReaderResolve().getPageSum());
                            mBookReadMenuView.setSeekBarProgress(pageIndex);
                        }
                    }
                    break;
                case READ_CHANGE_CHAPTER:
                    if (mBookReadMenuView != null) {
                        if (mIndexCardBean != null && mIndexCardBean.getChapters() != null && mIndexCardBean.getChapters().size() != 0) {
                            mBookReadMenuView.setBookChapterName(mIndexCardBean.getChapters().get(msg.arg1).getName());
                            mBookReadMenuView.setSeekBarMaxProgress(mReaderManager.getReaderResolve().getPageSum(), msg.arg2);
                        }
                    }
                    setSeletorChangeChapter(msg.arg1);
                    break;


            }
        }
    };

    @Override
    protected void initView() {
        initHistoryDialog();
        initDrawerAndChapter();
        SystemBarUtils.hideStableStatusBar(BookReadingActivity.this);
        mBookReadMenuView.setActivity(BookReadingActivity.this);
        Gson gson = new Gson();
        mIndexCardBean = gson.fromJson(getIntent().getStringExtra(Constant.BOOK_DATA), IndexCardBean.class);
        mReaderManager = new ReaderView.ReaderManager(mContext);
        mBookReadView.setReaderManager(mReaderManager);
        mAdapter = new BookContentAdapter();
        mBookReadView.setAdapter(mAdapter);
        mBookReadView.setTextSize(SPUtils.getInstance().getInt(Constant.FONT_SIZE, 50));
        if (SPUtils.getInstance().getBoolean(Constant.NIGHT_MDE_OPEN, false)) {
            if (mBookReadView != null) {
                mBookReadView.setBackgroundColor(getResources().getColor(R.color.colorNightBlack));
                mBookReadView.setColorsConfig(new ColorsConfig(getResources().getColor(R.color.colorNightGary), getResources().getColor(R.color.colorNightGary)));
            }
            if (mBookReadMenuView != null) {
                mBookReadMenuView.onShowReadBg(true);
            }
        } else {
            if (mBookReadView != null) {
                mBookReadView.setBackgroundColor(Color.parseColor(SPUtils.getInstance().getString(Constant.READ_BG, Constant.READ_BG_4)));
            }
            if (mBookReadMenuView != null) {
                mBookReadMenuView.initPageBg();
                mBookReadMenuView.onShowReadBg(false);
            }
        }
        mBookReadMenuView.setSeekBarTypefaceProgress(mBookReadView.getTextSize());
        mReaderManager.setOnReaderWatcherListener(this);
        if (mIndexCardBean != null) {
            mAdapter.setChapterList(mIndexCardBean.getChapters());
            mAdapter.notifyDataSetChanged();
            mBookReadMenuView.setBookName(mIndexCardBean.getName());
            if (mCatalogAdapter != null) {
                mBookChapterList.clear();
                mBookChapterList.addAll(mIndexCardBean.getChapters());
                mCatalogAdapter.setData(mBookChapterList);
            }
            if (getIntent().getIntExtra(Constant.PAGE_TYPE, 0) != 1) {
                openHistoryData();
            } else {
                skipChapter(getIntent().getIntExtra(Constant.BOOK_CHAPTER_POSITION, 0), ReaderResolve.FIRST_INDEX);
            }
            List<BookInfoBean> mBookrckList = BookInfoBean.findBookInfo(mIndexCardBean.getName());
            if (mBookrckList.size() != 0) {
                setJoinBookrackTag(true);
            } else {
                setJoinBookrackTag(false);
            }
        }
        mBookReadMenuView.setChapterCallBack(this);
        mBookReadMenuView.setCallBack(this);
    }

    /**
     * 设置加入书架 提示
     *
     * @param isSave
     */
    private void setJoinBookrackTag(boolean isSave) {
        if (mBookReadMenuView != null) {
            mBookReadMenuView.onAddBookrack(isSave);
        }
    }

    private void initHistoryDialog() {
        mHistoryDialogBuilder = new AlertDialog.Builder(mContext);
        mHistoryDialogBuilder.setMessage(getResources().getString(R.string.jump_history_data));
        mHistoryDialogBuilder.setNegativeButton(getResources().getText(R.string.cancel), (dialog, which) -> dialog.dismiss()).setPositiveButton(getResources().getText(R.string.jump), (dialog, which) -> BookReadingActivity.this.openHistoryData()).create();
    }

    /**
     * 打开历史
     */
    private void openHistoryData() {
        List<BookReadBean> mBookReadList = BookReadBean.onFindBookRead(mIndexCardBean.getName());
        if (mBookReadList != null && mBookReadList.size() != 0) {
            if (mReaderManager != null) {
                mReaderManager.startFromCache(mBookReadList.get(0).cacheKey, mBookReadList.get(0).chapterIndex, mBookReadList.get(0).charIndex, mBookReadList.get(0).chapterTitle, mReaderManager.getReaderResolve().getChapterSum());
                mReaderManager.toSpecifiedChapter(mBookReadList.get(0).chapterIndex, mBookReadList.get(0).charIndex, false);
            }
        }
    }

    /**
     * 展示状态栏
     *
     * @param isShow
     */
    private void showSystemBar(boolean isShow) {
        if (isShow) {
            QMUIStatusBarHelper.setStatusBarLightMode(this);
            SystemBarUtils.showUnStableStatusBar(this);
        } else {
            QMUIStatusBarHelper.setStatusBarDarkMode(this);
            SystemBarUtils.hideStableStatusBar(this);
        }
    }

    /**
     * 初始化抽屉页和目录页
     */
    private void initDrawerAndChapter() {
        //禁止滑动展示DrawerLayout
        mReadDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        //侧边打开后，返回键能够起作用
        mReadDrawer.setFocusableInTouchMode(false);
        mReadDrawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                isDrawerOpen = true;
                showSystemBar(true);

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                isDrawerOpen = false;
                showSystemBar(false);
            }

            @Override

            public void onDrawerStateChanged(int newState) {

            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(RecyclerView.VERTICAL);
        mReadChapterList.setLayoutManager(manager);
        mCatalogAdapter = new ChapterAdapter(R.layout.item_chapter, mBookChapterList);
        mCatalogAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                mReadDrawer.closeDrawer(GravityCompat.START);
                if (position == mReaderManager.getReaderResolve().getChapterIndex()) {
                    return;
                }
                BookReadingActivity.this.skipChapter(position, ReaderResolve.FIRST_INDEX);
            }
        });
        mReadChapterList.setAdapter(mCatalogAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mReaderManager != null) {
            mReaderManager.onDestoryDownListener();
        }

        onSaveBookReading();
        mHandler.removeMessages(READ_CHANGE_PAGE);
        mHandler.removeMessages(READ_CHANGE_CHAPTER);
    }

    /**
     * 保存阅读进度
     */
    private void onSaveBookReading() {
        if (mIndexCardBean == null) {
            return;
        }
        BookReadBean bookReadBean = new BookReadBean();
        bookReadBean.bookName = mIndexCardBean.name;
        bookReadBean.chapterIndex = mBookReadView.getReaderManager().getReaderResolve().getChapterIndex();
        bookReadBean.chapterTitle = mBookReadView.getReaderManager().getReaderResolve().getTitle();
        bookReadBean.charIndex = mBookReadView.getReaderManager().getReaderResolve().getCharIndex();
        bookReadBean.cacheKey = mBookChapterList.get(mBookReadView.getReaderManager().getReaderResolve().getChapterIndex()).url;
        BookReadBean.onSaveBookReading(bookReadBean);
    }

    /**
     * 章节跳转
     */
    private void skipChapter(int mChapterPos, int charIndex) {
        if (mChapterPos >= mReaderManager.getReaderResolve().getChapterSum()) {
            return;
        }
        if (mReaderManager != null) {
            mReaderManager.toSpecifiedChapter(mChapterPos, charIndex, true);
        }
        if (mBookReadView != null) {
            mBookReadView.invalidateBothPage();
        }
        setSeletorChangeChapter(mChapterPos);
    }

    /**
     * 设置切换目录时选中抽屉目录对应Item
     *
     * @param position
     */
    private void setSeletorChangeChapter(int position) {
        if (mBookChapterList != null && mBookChapterList.size() != 0) {
            if (mCatalogAdapter != null) {
                mCatalogAdapter.setSelectedPosition(position);
            }

        }
    }

    public static void onIntentPager(Activity activity, int chapterPosition, String bookData, int pageType) {
        Intent intent = new Intent(activity, BookReadingActivity.class);
        intent.putExtra(Constant.BOOK_CHAPTER_POSITION, chapterPosition);
        intent.putExtra(Constant.BOOK_DATA, bookData);
        intent.putExtra(Constant.PAGE_TYPE, pageType);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_book_read;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    public void onPageChanged(int pageIndex) {
        Message message = new Message();
        message.what = READ_CHANGE_PAGE;
        message.obj = pageIndex;
        mHandler.sendMessage(message);
    }

    @Override
    public void onChapterChanged(int chapterIndex, int pageIndex) {
        Message message = new Message();
        message.what = READ_CHANGE_CHAPTER;
        message.arg1 = chapterIndex;
        message.arg2 = pageIndex;
        mHandler.sendMessage(message);
    }

    @Override
    public void onChapterDownloadStart(int chapterIndex) {

    }

    @Override
    public void onChapterDownloadSuccess(int chapterIndex) {

    }

    @Override
    public void onChapterDownloadError(int chapterIndex) {

    }

    @Override
    public void onNextClick() {
        if (mReaderManager != null) {
            mReaderManager.toNextChapter();
        }
        if (mBookReadView != null) {
            mBookReadView.invalidateBothPage();
        }
    }

    @Override
    public void onLastClick() {
        if (mReaderManager != null) {
            mReaderManager.toPrevChapter(ReaderResolve.FIRST_INDEX);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.read_chapter_minus_typeface:
                mBookReadView.setTextSize(mBookReadView.getTextSize() - 1);
                mBookReadMenuView.getBookSeekBarTypefaceProgress().setProgress(mBookReadView.getTextSize());
                break;
            case R.id.read_chapter_add_typeface:
                mBookReadView.setTextSize(mBookReadView.getTextSize() + 1);
                mBookReadMenuView.getBookSeekBarTypefaceProgress().setProgress(mBookReadView.getTextSize());
                break;
            case R.id.book_reading_title_bar_ll:
                finish();
                break;
            case R.id.book_reading_catalogue_tv:
                mBookReadMenuView.dismiss();
                showSystemBar(true);
                mReadDrawer.openDrawer(GravityCompat.START);
                break;
        }
    }

    @Override
    public void onRadioChanged(int checkedId) {
        switch (checkedId) {
            case R.id.change_pager_mde_cover:
                if (mBookReadView != null) {
                    SPUtils.getInstance().put(Constant.EFFECT_TYPE, Constant.EFFECT_OF_COVER_PAGE);
                    mBookReadView.setEffect(new EffectOfCover(mContext));
                }
                break;
            case R.id.change_pager_mde_nothing:
                SPUtils.getInstance().put(Constant.EFFECT_TYPE, Constant.NOTHING_PAGE);
                if (mBookReadView != null) {
                    mBookReadView.setEffect(new EffectOfNon(mContext));
                }
                break;
            case R.id.change_pager_mde_slide:
                SPUtils.getInstance().put(Constant.EFFECT_TYPE, Constant.SLIDE_PAGE);

                if (mBookReadView != null) {
                    mBookReadView.setEffect(new EffectOfSlide(mContext));
                }
                break;
            case R.id.change_pager_mde_emulation:
                SPUtils.getInstance().put(Constant.EFFECT_TYPE, Constant.EMULATION_PAGE);
                if (mBookReadView != null) {
                    mBookReadView.setEffect(new EffectOfRealBothWay(mContext));
                }
                break;
            case R.id.change_pager_mde_default:
                SPUtils.getInstance().put(Constant.EFFECT_TYPE, Constant.DEFAULT_PAGE);
                if (mBookReadView != null) {
                    mBookReadView.setEffect(new EffectOfRealOneWay(mContext));
                }
                break;
            case R.id.change_pager_color_one:
                SPUtils.getInstance().put(Constant.READ_BG, Constant.READ_BG_0);
                if (mBookReadView != null) {
                    mBookReadView.setBackgroundColor(getResources().getColor(R.color.reader_bg_0));
                }
                break;
            case R.id.change_pager_color_two:
                SPUtils.getInstance().put(Constant.READ_BG, Constant.READ_BG_1);

                if (mBookReadView != null) {
                    mBookReadView.setBackgroundColor(getResources().getColor(R.color.reader_bg_1));
                }
                break;
            case R.id.change_pager_color_three:
                SPUtils.getInstance().put(Constant.READ_BG, Constant.READ_BG_2);
                if (mBookReadView != null) {
                    mBookReadView.setBackgroundColor(getResources().getColor(R.color.reader_bg_2));
                }
                break;
            case R.id.change_pager_color_four:
                SPUtils.getInstance().put(Constant.READ_BG, Constant.READ_BG_3);
                if (mBookReadView != null) {
                    mBookReadView.setBackgroundColor(getResources().getColor(R.color.reader_bg_3));
                }
                break;
            case R.id.change_pager_color_white:
                SPUtils.getInstance().put(Constant.READ_BG, Constant.READ_BG_4);
                if (mBookReadView != null) {
                    mBookReadView.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                }
                break;
            case R.id.book_reading_add_bookrack:

                break;
        }
    }

    /**
     * 加入书架
     *
     * @param isJoin
     */
    private void onJoinBookrack(boolean isJoin) {
        if (mIndexCardBean == null) {
            return;
        }
        if (!isJoin) {
            List<BookInfoBean> mBookInfoList = BookInfoBean.findBookInfo(mIndexCardBean.getName());
            if (mBookInfoList != null && mBookInfoList.size() != 0) {
                BookInfoBean.deleteJoinBookrack(mIndexCardBean.getName(), new UpdateOrDeleteCallback() {
                    @Override
                    public void onFinish(int rowsAffected) {
                    }
                });
            }
        } else {
            List<BookInfoBean> mBookInfoList = BookInfoBean.findBookInfo(mIndexCardBean.getName());
            if (mBookInfoList != null && mBookInfoList.size() != 0) {
                BookInfoBean.updateBookrack(mIndexCardBean, new UpdateOrDeleteCallback() {
                    @Override
                    public void onFinish(int rowsAffected) {
                    }
                });
            } else {
                BookInfoBean.saveBookrack(mIndexCardBean, new SaveCallback() {
                    @Override
                    public void onFinish(boolean success) {
                    }
                });
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.read_sb_typeface_progress:
                if (mBookReadMenuView.getBookSeekBarTypefaceProgress() != null) {
                    if (progress <= 15) {
                        mBookReadView.setTextSize(15);
                        SPUtils.getInstance().put(Constant.FONT_SIZE, 15);
                    } else {
                        mBookReadView.setTextSize(progress);
                        SPUtils.getInstance().put(Constant.FONT_SIZE, progress);
                    }

                }
                break;
            default:
                if (mBookReadMenuView.isShowing()) {
                    mBookReadMenuView.getBookSeekBarProgress().setMax(Math.max(0, mReaderManager.getReaderResolve().getPageSum() - 1));
                    mReaderManager.toSkipPage(progress);
                    mBookReadMenuView.setBookChapterProgress((progress + 1) + "/" + mReaderManager.getReaderResolve().getPageSum());
                    mBookReadView.invalidateBothPage();
                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(int viewId, boolean isChecked) {
        if (viewId == R.id.book_reading_add_bookrack) {
            if (mBookReadMenuView != null) {
                mBookReadMenuView.getAddBookrack().setText(getResources().getString(isChecked == true ? R.string.jion_bookrack_s : R.string.jion_bookrack));
            }
            onJoinBookrack(isChecked);
        } else {
            onChangeNightMode(isChecked);
        }
    }

    /**
     * 切换 夜间模式
     *
     * @param isChecked
     */
    private void onChangeNightMode(boolean isChecked) {
        if (isChecked) {
            if (mBookReadView != null) {
                mBookReadView.setBackgroundColor(getResources().getColor(R.color.colorNightBlack));
                mBookReadView.setColorsConfig(new ColorsConfig(getResources().getColor(R.color.colorNightGary), getResources().getColor(R.color.colorNightGary)));
            }
            SkinCompatManager.getInstance().loadSkin("night", null, SkinCompatManager.SKIN_LOADER_STRATEGY_BUILD_IN);
        } else {
            SkinCompatManager.getInstance().restoreDefaultTheme();
            if (mBookReadView != null) {
                mBookReadView.setBackgroundColor(Color.parseColor(SPUtils.getInstance().getString(Constant.READ_BG, Constant.READ_BG_4)));
                mBookReadView.setColorsConfig(new ColorsConfig(getResources().getColor(R.color.colorBlack), getResources().getColor(R.color.colorBlack)));
            }
            if (mBookReadMenuView != null) {
                mBookReadMenuView.initPageBg();
            }
        }
        if (mBookReadMenuView != null) {
            mBookReadMenuView.onShowReadBg(isChecked);
            mBookReadMenuView.getModeChangeBox().setText(getResources().getString(isChecked == true ? R.string.night_mode : R.string.high_mode));
        }
        SPUtils.getInstance().put(Constant.NIGHT_MDE_OPEN, isChecked);
    }
}