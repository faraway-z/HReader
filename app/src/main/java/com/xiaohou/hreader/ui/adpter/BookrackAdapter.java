package com.xiaohou.hreader.ui.adpter;

import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.daimajia.swipe.SwipeLayout;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.utils.GlideRoundTransform;

import java.util.List;

public class BookrackAdapter extends BaseQuickAdapter<BookInfoBean, BaseViewHolder> {
    private List<BookInfoBean> data;
    private boolean isDeleteShow = false;

    public void setDeleteShow(boolean deleteShow) {
        isDeleteShow = deleteShow;
        notifyDataSetChanged();
    }

    public void setData(List<BookInfoBean> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public BookrackAdapter(int layoutResId, @Nullable List<BookInfoBean> data) {
        super(layoutResId, data);
        this.data = data;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, BookInfoBean item) {
        Button mDeleteBtn = helper.getView(R.id.item_bookrack_delete_btn);
        mDeleteBtn.setVisibility(isDeleteShow == true? View.VISIBLE:View.GONE);
        helper.setText(R.id.item_bookrack_desc, Html.fromHtml(item.getInfo()))
                .addOnClickListener(R.id.item_bookrack_box,R.id.item_bookrack_delete_btn)
                .addOnLongClickListener(R.id.item_bookrack_box)
                .setText(R.id.item_bookrack_name, item.getName());
        Glide.with(mContext).load(item.getImgUrl()).apply(new RequestOptions().transform(new GlideRoundTransform(5))).into((ImageView) helper.getView(R.id.item_bookrack_img));
    }
}
