package com.xiaohou.hreader.ui.widget.reader.widget;

import com.xiaohou.hreader.request.base.RequestManager;
import com.xiaohou.hreader.request.base.RequestSubscribe;
import com.xiaohou.hreader.request.base.RxThreadUtil;

public class ReaderContentUtlis {

    private static String bookContent;

    public static void download(String request,RequestSubscribe subscribe) {
        RequestManager.getInstance().getApi.getContentData(request)
                .compose(RxThreadUtil.rxObservableSchedulerHelper())
                .subscribe(subscribe);
    }
}
