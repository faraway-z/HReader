package com.xiaohou.hreader.ui.adpter;

import com.xiaohou.hreader.Constant;
import com.xiaohou.hreader.request.Request;
import com.xiaohou.hreader.request.bean.BookChapterBean;
import com.xiaohou.hreader.request.bean.BookContentBean;
import com.xiaohou.hreader.ui.widget.reader.widget.ReaderView;

public class BookContentAdapter extends ReaderView.Adapter<BookChapterBean, BookContentBean>{

    @Override
    public String obtainCacheKey(BookChapterBean bookChapterBean) {
        return bookChapterBean.getUrl();
    }

    @Override
    public String obtainChapterName(BookChapterBean bookChapterBean) {
        return bookChapterBean.getName();

    }

    @Override
    public String obtainChapterContent(BookContentBean bookContentBean) {
        return bookContentBean.getContent();
    }

    @Override
    public BookContentBean downLoad(BookChapterBean bookChapterBean) {
        return null;
    }

    @Override
    public Request requestParams(BookChapterBean bookChapterBean) {
        return new Request.Builder().baseUrl(Constant.BASE_URL + bookChapterBean.getUrl())
                .get().build();
    }
}
