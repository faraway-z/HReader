package com.xiaohou.hreader.ui.adpter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.request.bean.BookInfoBean;

import java.util.List;

public class NewBookItemAdapter extends BaseQuickAdapter<BookInfoBean, BaseViewHolder> {
    public List<BookInfoBean> data;
    public NewBookItemAdapter(int layoutResId, @Nullable List<BookInfoBean> data) {
        super(layoutResId, data);
        this.data =data;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, BookInfoBean item) {
        helper.setText(R.id.item_new_book_content_book_name,item.getName())
                .addOnClickListener(R.id.item_new_book_content_book_box)
                .setText(R.id.item_new_book_content_book_author,item.getAuthor());
    }
}
