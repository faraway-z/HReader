package com.xiaohou.hreader.ui.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.SPUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xiaohou.hreader.Constant;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.ui.adpter.IndexAdapter;
import com.xiaohou.hreader.ui.adpter.ItemMultiItemEntity;
import com.xiaohou.hreader.request.bean.IndexBean;
import com.xiaohou.hreader.request.mvp.index.IndexConcrat;
import com.xiaohou.hreader.request.mvp.index.IndexPersenter;
import com.xiaohou.hreader.ui.activity.BookDetailsActivity;
import com.xiaohou.hreader.ui.base.BaseMVPFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class IndexFragment extends BaseMVPFragment<IndexPersenter> implements IndexConcrat.view, OnRefreshListener {

    private static final String TAG = "IndexFragment: ";
    @BindView(R.id.index_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.index_smart_refresh)
    SmartRefreshLayout mSmartRefresh;
    private IndexPersenter mIndexPersenter;
    private IndexBean mIndexBean = new IndexBean();
    private IndexAdapter mIndexAdapter;
    private RefreshLayout mRefreshLayout;

    @Override
    protected void lazyLoadShow() {
        mIndexPersenter.getIndexData();
    }

    @Override
    public void onStop() {
        super.onStop();
        SPUtils.getInstance().put(Constant.CHARSET,Constant.GBK);
    }

    @Override
    protected void initView() {
        super.initView();
        mSmartRefresh.setOnRefreshListener(this);
        mSmartRefresh.setEnableLoadMore(false);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        List<ItemMultiItemEntity> multiItemEntities = new ArrayList<>();
        multiItemEntities.add(new ItemMultiItemEntity(ItemMultiItemEntity.INDEX_MULTI_ITEM_BANNER));
        multiItemEntities.add(new ItemMultiItemEntity(ItemMultiItemEntity.INDEX_MULTI_ITEM_CARD));
        multiItemEntities.add(new ItemMultiItemEntity(ItemMultiItemEntity.INDEX_MULTI_ITEM_NEW));
        multiItemEntities.add(new ItemMultiItemEntity(ItemMultiItemEntity.INDEX_MULTI_ITEM_NEW_JION));
        multiItemEntities.add(new ItemMultiItemEntity(ItemMultiItemEntity.INDEX_MULTI_ITEM_NO_DATA));
        mIndexAdapter = new IndexAdapter(multiItemEntities,mContext,mIndexBean);
        mIndexAdapter.setOnItemClickListener(new IndexAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BookInfoBean item) {
                BookDetailsActivity.onIntentPager(IndexFragment.this.getActivity(), item.getRoute(), false);
            }
        });
        mRecyclerView.setAdapter(mIndexAdapter);
    }

    public static IndexFragment newInstance(String typeName) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(TYPE_NAME, typeName);
        IndexFragment fragment = new IndexFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void lazyLoadHide() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_index;
    }

    @Override
    public void onSuccess(IndexBean o) {
        mIndexBean = o;
        mIndexAdapter.setDataBean(mIndexBean);
    }

    @Override
    protected IndexPersenter createPresenter() {
        mIndexPersenter = new IndexPersenter();
        return mIndexPersenter;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {
        if (mRefreshLayout!=null){
            mRefreshLayout.finishRefresh();
        }
    }

    @Override
    public void onError(Object o) {

    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        mRefreshLayout = refreshLayout;
        mIndexPersenter.getIndexData();
    }
}
