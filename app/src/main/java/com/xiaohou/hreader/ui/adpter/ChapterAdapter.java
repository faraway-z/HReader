package com.xiaohou.hreader.ui.adpter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.request.bean.BookChapterBean;

import java.util.List;

public class ChapterAdapter extends BaseQuickAdapter<BookChapterBean, BaseViewHolder> {
    private List<BookChapterBean> data;
    private int mSelectedPosition = 0;

    public void setSelectedPosition(int mSelectedPosition) {
        this.mSelectedPosition = mSelectedPosition;
        notifyDataSetChanged();
    }

    public void setData(List<BookChapterBean> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public ChapterAdapter(int layoutResId, @Nullable List<BookChapterBean> data) {
        super(layoutResId, data);
        this.data = data;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, BookChapterBean item) {
        helper.setTextColor(R.id.iem_chapter_name,mContext.getResources().getColor(helper.getPosition() == mSelectedPosition ?R.color.colorPink:R.color.colorBlack_01)).setText(R.id.iem_chapter_name,(helper.getPosition()+1)+".  "+item.getName()).addOnClickListener(R.id.item_chapter_box);
    }
}
