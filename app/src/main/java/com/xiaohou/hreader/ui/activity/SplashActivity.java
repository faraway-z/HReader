package com.xiaohou.hreader.ui.activity;

import android.content.Intent;
import android.os.Handler;

import com.xiaohou.hreader.R;
import com.xiaohou.hreader.ui.base.BaseActivity;

public class SplashActivity extends BaseActivity {
    @Override
    protected void initView() {
        new Handler().postDelayed(() -> {
            startActivity(new Intent(SplashActivity.this, IndexActivity.class));
            finish();
        },1000);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_splash;
    }
}
