package com.xiaohou.hreader.ui.adpter;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.request.bean.IndexCardBean;

import java.util.List;

public class NewBookAdapter extends BaseQuickAdapter<IndexCardBean, BaseViewHolder> {
    public Context mContext;
    private OnItemClickListener onItemClickListener =null;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public NewBookAdapter(int layoutResId, @Nullable List<IndexCardBean> data, Context mContext) {
        super(layoutResId, data);
        this.mContext = mContext;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, final IndexCardBean item) {
        helper.setText(R.id.item_new_book_type,item.getTypeName());
        RecyclerView mRecyclerView = helper.getView(R.id.item_new_book_recycler_view);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        NewBookItemAdapter newBookItemAdapter = new NewBookItemAdapter(R.layout.item_new_book_content,item.getRecommendInfo());
        newBookItemAdapter.setOnItemChildClickListener(new OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(item.getRecommendInfo().get(position));
                }
            }
        });
        mRecyclerView.setAdapter(newBookItemAdapter);

    }

    /**
     * Item 点击事件
     */
    public interface OnItemClickListener {
        /**
         * 点击事件
         *
         * @param view view id
         * @param bookInfoBean
         */
        void onItemClick(BookInfoBean bookInfoBean);
    }
}
