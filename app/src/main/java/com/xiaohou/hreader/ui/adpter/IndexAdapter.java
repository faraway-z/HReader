package com.xiaohou.hreader.ui.adpter;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.request.bean.IndexBean;
import com.youth.banner.Banner;
import com.youth.banner.indicator.CircleIndicator;
import com.youth.banner.listener.OnBannerListener;

import java.util.List;

public class IndexAdapter extends BaseMultiItemQuickAdapter<ItemMultiItemEntity, BaseViewHolder> {


    private Context mContext;
    private IndexOtherBookAdapter mIndexOtherBookAdapter;
    private NewBookAdapter newBookAdapter;
    private NewBookAdapter newJionBookAdapter;

    public void setDataBean(IndexBean mDataBean) {
        this.mDataBean = mDataBean;
        notifyDataSetChanged();
    }
    private OnItemClickListener onItemClickListener =null;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private IndexBean mDataBean;
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public IndexAdapter(List<ItemMultiItemEntity> data, Context mContext, IndexBean mDataBean) {
        super(data);
        this.mContext = mContext;
        this.mDataBean = mDataBean;
        addItemType(ItemMultiItemEntity.INDEX_MULTI_ITEM_BANNER,R.layout.item_index_banner);
        addItemType(ItemMultiItemEntity.INDEX_MULTI_ITEM_CARD,R.layout.item_index_card);
        addItemType(ItemMultiItemEntity.INDEX_MULTI_ITEM_NEW,R.layout.item_index_card);
        addItemType(ItemMultiItemEntity.INDEX_MULTI_ITEM_NEW_JION,R.layout.item_index_card);
        addItemType(ItemMultiItemEntity.INDEX_MULTI_ITEM_NO_DATA,R.layout.item_no_data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, ItemMultiItemEntity item) {
        switch (item.getItemType()){
            case ItemMultiItemEntity.INDEX_MULTI_ITEM_BANNER:
                initBanner(helper);
                break;
            case ItemMultiItemEntity.INDEX_MULTI_ITEM_CARD:
                initCardRecyclerView(helper);
                break;
            case ItemMultiItemEntity.INDEX_MULTI_ITEM_NEW_JION:
            case ItemMultiItemEntity.INDEX_MULTI_ITEM_NEW:
                initNewRecyclerView(helper,item.getItemType());
                break;
             case ItemMultiItemEntity.INDEX_MULTI_ITEM_NO_DATA:
                initNoData(helper);
                break;

        }
    }

    private void initNoData(BaseViewHolder helper) {
        ImageView mLoading = helper.getView(R.id.item_no_data_loading);
        RelativeLayout mLoadingBox = helper.getView(R.id.item_loading_box);
        TextView mLoadingAllData = helper.getView(R.id.item_loading_all_data);
        RotateAnimation rotate  = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        LinearInterpolator lin = new LinearInterpolator();
        rotate.setInterpolator(lin);
        rotate.setDuration(2000);//设置动画持续周期
        rotate.setRepeatCount(-1);//设置重复次数
        rotate.setFillAfter(true);//动画执行完后是否停留在执行完的状态
        if (mDataBean.isLoading()){
            mLoadingBox.setVisibility(View.VISIBLE);
            mLoadingAllData.setVisibility(View.GONE);
            mLoading.startAnimation(rotate);
        }else {
            mLoadingBox.setVisibility(View.GONE);
            mLoadingAllData.setVisibility(View.VISIBLE);
            mLoading.clearAnimation();
        }



    }

    /**
     * 初始化 最小 小说 列表
     * @param helper
     * @param itemType
     */
    private void initNewRecyclerView(BaseViewHolder helper, int itemType) {
        RecyclerView mRecyclerView = helper.getView(R.id.item_index_recycler_view);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        if (itemType == ItemMultiItemEntity.INDEX_MULTI_ITEM_NEW){
            newBookAdapter = new NewBookAdapter(R.layout.item_new_book,mDataBean.getNewBookList(),mContext);
            newBookAdapter.setOnItemClickListener(new NewBookAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BookInfoBean infoBean) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(infoBean);
                    }
                }
            });
            mRecyclerView.setAdapter(newBookAdapter);
        }else {
            newJionBookAdapter = new NewBookAdapter(R.layout.item_new_book, mDataBean.getNewStorageBookList(), mContext);
            newJionBookAdapter.setOnItemClickListener(new NewBookAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BookInfoBean infoBean) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(infoBean);
                    }
                }
            });
            mRecyclerView.setAdapter(newJionBookAdapter);
        }

    }

    /**
     * 初始化Card RecyclerView
     * @param helper
     */
    private void initCardRecyclerView(BaseViewHolder helper) {
        RecyclerView mRecyclerView = helper.getView(R.id.item_index_recycler_view);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(RecyclerView.HORIZONTAL);
        mRecyclerView.setLayoutManager(manager);
        mIndexOtherBookAdapter = new IndexOtherBookAdapter(R.layout.item_index_card_info,mDataBean.getCardList(), mContext);
        mIndexOtherBookAdapter.setOnItemChildClickListener(new OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(mDataBean.getCardList().get(position));
                }
            }
        });
        mIndexOtherBookAdapter.setOnItemClickListener(new IndexOtherBookAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BookInfoBean bean) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(bean);
                }
            }
        });
        mRecyclerView.setAdapter(mIndexOtherBookAdapter);
    }

    /**
     * Banner
     * @param helper
     */
    private void initBanner(BaseViewHolder helper) {
        Banner banner = helper.getView(R.id.item_index_banner);
        BannerAdapter mBannerAdapter = new BannerAdapter(mContext,mDataBean.getBannerList());
        banner.setAdapter(mBannerAdapter)
                .setOrientation(Banner.HORIZONTAL)
                .setIndicator(new CircleIndicator(mContext))
                .setOnBannerListener(new OnBannerListener<BookInfoBean>() {
                    @Override
                    public void OnBannerClick(BookInfoBean data, int position) {
                        if (onItemClickListener!=null){
                            onItemClickListener.onItemClick(data);
                        }
                    }

                    @Override
                    public void onBannerChanged(int position) {

                    }
                })
                .setUserInputEnabled(false);
        banner.start();
    }
    /**
     * Item 点击事件
     */
    public interface OnItemClickListener{

        /**
         * 点击事件
         * @param item
         */
        void onItemClick(BookInfoBean item);
    }
}


