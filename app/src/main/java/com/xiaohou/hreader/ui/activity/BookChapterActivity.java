package com.xiaohou.hreader.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.xiaohou.hreader.Constant;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.ui.adpter.ChapterAdapter;
import com.xiaohou.hreader.request.bean.IndexCardBean;
import com.xiaohou.hreader.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookChapterActivity extends BaseActivity {

    @BindView(R.id.navigation_bar_title)
    TextView mNavigationBarTitle;

    @BindView(R.id.book_chapter_recycler_view)
    RecyclerView mBookChapterRecyclerView;
    private IndexCardBean indexCardBean;
    private ChapterAdapter mChapterAdapter;

    public static void onStartPager(Activity activity, String bookDetails) {
        Intent intent = new Intent(activity,BookChapterActivity.class);
        intent.putExtra(Constant.BOOK_DATA,bookDetails);
        activity.startActivity(intent);
    }

    @Override
    protected void initView() {

        Gson gson = new Gson();
        indexCardBean = gson.fromJson(getIntent().getStringExtra(Constant.BOOK_DATA), IndexCardBean.class);
        if (indexCardBean ==null){
            return;
        }
        mNavigationBarTitle.setText(indexCardBean.getName());
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(RecyclerView.VERTICAL);
        mBookChapterRecyclerView.addItemDecoration(new DividerItemDecoration(mContext,DividerItemDecoration.VERTICAL));
        mBookChapterRecyclerView.setLayoutManager(manager);
        mChapterAdapter = new ChapterAdapter(R.layout.item_chapter,indexCardBean.getChapters());
        mChapterAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                mChapterAdapter.setSelectedPosition(position);
                BookReadingActivity.onIntentPager(BookChapterActivity.this, position, BookChapterActivity.this.getIntent().getStringExtra(Constant.BOOK_DATA), 1);
            }
        });
        mBookChapterRecyclerView.setAdapter(mChapterAdapter);

    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_book_chapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.navigation_bar_title_back)
    public void onViewClicked() {
        finish();
    }
}
