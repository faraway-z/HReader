package com.xiaohou.hreader.ui.adpter;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xiaohou.hreader.request.bean.BookInfoBean;
import com.xiaohou.hreader.utils.GlideRoundTransform;

import java.util.List;

public class BannerAdapter  extends com.youth.banner.adapter.BannerAdapter<BookInfoBean, BannerAdapter.BannerViewHolder> {
    public Context mContext;
    public BannerAdapter(Context mContext, List<BookInfoBean> mDatas)
    {
        super(mDatas);
        this.mContext = mContext;
    }

    @Override
    public BannerViewHolder onCreateHolder(ViewGroup parent, int viewType) {
        ImageView imageView = new ImageView(mContext);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        imageView.setAdjustViewBounds(true);
        return new BannerViewHolder(imageView);
    }

    @Override
    public void onBindView(BannerViewHolder holder, BookInfoBean data, int position, int size) {
        Glide.with(mContext).load(data.getImgUrl()).apply(new RequestOptions().transform(new GlideRoundTransform(5))).into(holder.imageView);
    }

    class BannerViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public BannerViewHolder(@NonNull ImageView view) {
            super(view);
            this.imageView = view;
        }
    }
}
