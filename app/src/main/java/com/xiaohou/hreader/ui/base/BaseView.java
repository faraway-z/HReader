package com.xiaohou.hreader.ui.base;
public interface BaseView {
    void onNetWorkError();
    void showLoading();
    void hideLoading();
    void onError(Object o);
    void onPlaceholderView();
}
