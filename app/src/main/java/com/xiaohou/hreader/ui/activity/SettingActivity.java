package com.xiaohou.hreader.ui.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.qmuiteam.qmui.widget.grouplist.QMUICommonListItemView;
import com.tencent.bugly.beta.Beta;
import com.xiaohou.hreader.Constant;
import com.xiaohou.hreader.R;
import com.xiaohou.hreader.ui.base.BaseActivity;
import com.xiaohou.hreader.ui.widget.SwitchButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import skin.support.SkinCompatManager;

public class SettingActivity extends BaseActivity {
    private static final String TAG = "SettingActivity：";
    @BindView(R.id.navigation_bar_title)
    TextView mNavigationBarTitle;
    @BindView(R.id.setting_versions_text)
    TextView mVersionsText;
    @BindView(R.id.setting_night_mode_switch_button)
    SwitchButton mNightModeSwitchButton;
    private AlertDialog dialog;
    private AlertDialog.Builder builder;

    @Override
    protected void initView() {
        mNavigationBarTitle.setText(getResources().getString(R.string.setting));
        mVersionsText.setText(AppUtils.getAppVersionName());
        mNightModeSwitchButton.setBackgroundColorUnchecked(getResources().getColor(R.color.colorGray));
        mNightModeSwitchButton.setBackgroundColorChecked(getResources().getColor(R.color.colorPink));
        mNightModeSwitchButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SPUtils.getInstance().put(Constant.NIGHT_MDE_OPEN, isChecked);
            if (isChecked) {
                SkinCompatManager.getInstance().loadSkin("night", null, SkinCompatManager.SKIN_LOADER_STRATEGY_BUILD_IN);
            } else {
                SkinCompatManager.getInstance().restoreDefaultTheme();
            }
        });
        mNightModeSwitchButton.setChecked(SPUtils.getInstance().getBoolean(Constant.NIGHT_MDE_OPEN, false));
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_setting;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.navigation_bar_title_back, R.id.setting_update_app_box, R.id.setting_copyright_box})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.setting_update_app_box:
                Beta.checkUpgrade(true, false);
                break;
            case R.id.setting_copyright_box:
                onShowCopyrightMsgDailog();
                break;
            default:
                finish();
                break;
        }
    }

    private void onShowCopyrightMsgDailog() {
        if (builder == null) {
            builder = new AlertDialog.Builder(mContext);
            builder.setMessage(getResources().getString(R.string.copyright_msg))
                    .setNegativeButton(getResources().getString(R.string.confirm), (dialog, which) -> dialog.dismiss());
        }
        if (dialog==null) {
            dialog = builder.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
