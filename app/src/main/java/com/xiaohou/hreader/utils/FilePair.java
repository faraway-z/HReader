package com.xiaohou.hreader.utils;

/**
 * Created by Garrett on 2018/11/22.
 * contact me krouky@outlook.com
 */
public class FilePair {
    public  String mFileName;
    public byte[] mBinaryData;

    public FilePair(String fileName, byte[] data) {
        this.mFileName = fileName;
        this.mBinaryData = data;
    }
}
